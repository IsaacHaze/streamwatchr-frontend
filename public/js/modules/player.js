define(['angular', 'jquery'], function(angular, jQuery) {
  'use strict';

  angular.module('streamwatchr.player', ['youtube-embed'])
    .controller('PlayerController', ['$scope', '$window', 'Player', 'ImagePreload', 'IlpsLogging', function($scope, $window, Player, ImagePreload, IlpsLogging) {
      $scope.state = Player.state;
      $scope.youtube = {
        player: null,
        videoId: null,
        playerVars: {
          autoplay: 1,
          controls: 0,
          fs: 1,
          showinfo: 0,
          iv_load_policy: 3,
          enablejsapi: 1
        },
        videoShown: false
      };

      $scope.toggleVideo = function() {
        if ($scope.youtube.videoShown) {
          $scope.youtube.videoShown = false;

          IlpsLogging.logEvent('toggle_video', { show: false });
          var logState = IlpsLogging.getState();
          logState.player_video_shown = false;
          IlpsLogging.setState(logState);
        } else {
          $scope.youtube.videoShown = true;
          
          IlpsLogging.logEvent('toggle_video', { show: true });
          var logState = IlpsLogging.getState();
          logState.player_video_shown = true;
          IlpsLogging.setState(logState);
        }
      };

      $scope.togglePlay = function () {
        if($scope.state.playState == Player.possiblePlayStates.PLAYING) {
          Player.pausePlayer();
          
          IlpsLogging.logEvent('pause_song', {
              song: Player.state.currentSong.song,
              artist: Player.state.currentSong.artist,
              mbId: Player.state.currentSong.mbId,
              video_id: Player.state.currentSong.videoId,
              initiated_from: 'player_button',
              song_duration_s: Player.state.currentSong.duration_s,
              song_elapsed_s: Player.state.currentSong.elapsed_s,
              percentage_played: (Player.state.currentSong.elapsed_s / Player.state.currentSong.duration_s) * 100
          });
        } else {
          Player.resumePlayer();

          IlpsLogging.logEvent('resume_song', {
              song: Player.state.currentSong.song,
              artist: Player.state.currentSong.artist,
              mbId: Player.state.currentSong.mbId,
              video_id: Player.state.currentSong.videoId,
              initiated_from: 'player_button',
              song_duration_s: Player.state.currentSong.duration_s,
              song_elapsed_s: Player.state.currentSong.elapsed_s,
              percentage_played: (Player.state.currentSong.elapsed_s / Player.state.currentSong.duration_s) * 100
          });
        }
      };

      $scope.tweetCurrentSong = function () {
        var loc = 'http://youtu.be/' + $scope.state.currentSong.video_id;
        var title = encodeURIComponent('#np ' + unescape($scope.state.currentSong.artist) + ' - ' + unescape($scope.state.currentSong.song));
        var via = "streamwatchr";
        var hashtags = "swtchr";

        var url = 'http://twitter.com/share?url=' + loc + '&text=' + title + '&via='  + via + '&hashtags=' + hashtags;
        var windowOptions = 'height=450, width=550, top=' + ($window.screen.height / 2 - 225) +', left=' + ($window.screen.width / 2) +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0';
        
        $window.open(url, 'twitter_share_window', windowOptions);
      };

      $scope.playRecommendedNext = function() {
        Player.playSong(Player.state.nextSong);
      };

      $scope.goToChordify = function() {
        if (!('chordifyUrl' in $scope.state.currentSong) || !$scope.state.currentSong.chordifyUrl) {
          return;
        }

        Player.pausePlayer();
        $window.open($scope.state.currentSong.chordifyUrl, '_blank');

        var elapsed_s = $scope.youtube.player.getCurrentTime();
        var duration_s = $scope.youtube.player.getDuration();
        IlpsLogging.logEvent('chordify_click', {
          song_duration_s: duration_s,
          song_elapsed_s: elapsed_s,
          percentage_played: (elapsed_s / duration_s) * 100,
        })
      };

      $scope.$watch('state.currentSong', function(newSong, oldSong) {
        if (newSong.video_id !== oldSong.video_id) {
          if (oldSong && $scope.youtube.player) {
            var elapsed_s = $scope.youtube.player.getCurrentTime();
            var duration_s = $scope.youtube.player.getDuration();

            if (elapsed_s !== duration_s) {
              IlpsLogging.logEvent('song_ended', {
                song: oldSong.song,
                artist: oldSong.artist,
                mbId: oldSong.mbId,
                video_id: oldSong.video_id,
                song_duration_s: duration_s,
                song_elapsed_s: elapsed_s,
                percentage_played: (elapsed_s / duration_s) * 100,
                skip_to_recommended: newSong.recommended ? true : false
              });
            }
          }

          $scope.youtube.videoId = newSong.video_id;
          ImagePreload.preload(newSong.images[2]['#text']).then(function(data) {
            // Pass the image's actual dimensions (used to determine styling)
            $scope.state.currentSong.images[2].width = data.width;
            $scope.state.currentSong.images[2].height = data.height;

          });
        }
      });

      $scope.$watch('state.playState', function(newState, oldState) {
        if (newState == Player.possiblePlayStates.PAUSED) {
          $scope.youtube.player.pauseVideo();
        }

        if (newState == Player.possiblePlayStates.PLAYING) {
          $scope.youtube.player.playVideo();
        }
      });

      $scope.$on('youtube.player.playing', function($event, player) {
        if ($scope.state.playState !== Player.possiblePlayStates.PLAYING) {
          $scope.state.playState = Player.possiblePlayStates.PLAYING;
        }

        Player.setCurrentSongDuration(player.getDuration());

        var logState = IlpsLogging.getState();
        logState.currently_playing = {
          song: Player.state.currentSong.song,
          artist: Player.state.currentSong.artist,
          mbId: Player.state.currentSong.mbId,
          video_id: Player.state.currentSong.video_id,
          state: 'playing'
        };
        IlpsLogging.setState(logState);
      });

      $scope.$on('youtube.player.paused', function($event, player) {
        Player.setElapsedTime(player.getCurrentTime());

        if ($scope.state.playState !== Player.possiblePlayStates.PAUSED) {
          Player.pausePlayer();
        }

        var logState = IlpsLogging.getState();
        logState.currently_playing.state = 'paused';
        IlpsLogging.setState(logState);
      });

      $scope.$on('youtube.player.ended', function($event, player) {
        var logState = IlpsLogging.getState();
        logState.currently_playing = {};
        IlpsLogging.setState(logState);

        IlpsLogging.logEvent('song_finished', {
          song: Player.state.currentSong.song,
          artist: Player.state.currentSong.artist,
          mbId: Player.state.currentSong.mbId,
          video_id: Player.state.currentSong.videoId
        });

        // When the current video has finished playing, start the next
        // recommended song
        if (Player.state.nextSong) {
          Player.playSong(Player.state.nextSong);
        }
      });

      $scope.$on('youtube.player.error', function($event, player, error) {
        IlpsLogging.logEvent('youtube_error', {
          song: Player.state.currentSong.song,
          artist: Player.state.currentSong.artist,
          mbId: Player.state.currentSong.mbId,
          video_id: Player.state.currentSong.videoId,
          error_code: error.data
        });
      });
    }])
    .factory('Player', function($http, $q) {
      var player = null;
      var possiblePlayStates = {
        UNSTRATED: 0,
        PLAYING: 1,
        PAUSED: 2
      };
      var state = {
        playState: 0,
        currentSong: {},
        nextSong: {},
        requestingRecommendedNext: false
      };

      var defferedRecRequest = null;

      function getRecommendedNext(current_song_data) {
        state.requestingRecommendedNext = true;

        // Cancel pending recommendation requests
        if (defferedRecRequest) {
          defferedRecRequest.resolve();
        }
        defferedRecRequest = $q.defer();

        // We use jQuery's 'param' function to construct the URL params,
        // since Angular's native 'params' arg. doesn't result in a format
        // that our backend understands
        var params = jQuery.param({
          song: unescape(current_song_data.song),
          artists: current_song_data.mbId
        });

        var request = $http({
          url: '/recommend-radio?' + params,
          timeout: defferedRecRequest.promise
        });

        request.then(function(success) {
          var nextSong;
          for (var i=0; i< success.data.docs.length; i++) {
            if (!success.data.docs[i].youtubeID) {
              continue;
            }

            nextSong = {
              artist: success.data.docs[i].target.artists[0].name,
              song: success.data.docs[i].target.song,
              images: success.data.docs[i].image,
              mbId: success.data.docs[i].target.artists,
              video_id: success.data.docs[i].youtubeID
            };
            
            break;
          }
          
          if (nextSong) {
            state.nextSong = nextSong;
            state.nextSong.recommended = true;
            getChordifyUrl(nextSong).then(function(success) {
              state.nextSong.chordifyUrl = success.data.chordifyUrl;
            });
          } else {
            state.nextSong = {};
          }

          return success;
        });

        // Clean up the deffered promise
        request['finally'](function() {
          defferedRecRequest = null;
          state.requestingRecommendedNext = false;
        });

        return request;
      }

      function getChordifyUrl(song_data) {
        if (!song_data.video_id) {
          return $q.defer().resolve(false);
        }
        else {
          return $http.get('/api/chordify-url/' + song_data.video_id);
        }
      }

      function playSong(song_data) {
        state.currentSong = song_data;
        state.currentSong.duration_s = null;
        state.currentSong.elapsed_s = null;
        getRecommendedNext(song_data);

        if (!('chordifyUrl' in song_data)) {
          getChordifyUrl(song_data).then(function(success) {
            state.currentSong.chordifyUrl = success.data.chordifyUrl;
          });
        }
      }

      function resumePlayer() {
        state.playState = possiblePlayStates.PLAYING;
      }

      function pausePlayer() {
        state.playState = possiblePlayStates.PAUSED;
      }

      function setCurrentSongDuration(duration_s) {
        state.currentSong.duration_s = duration_s;
      }

      function setElapsedTime(elapsed_s) {
        state.currentSong.elapsed_s = elapsed_s;
      }

      return {
        state: state,
        possiblePlayStates: possiblePlayStates,
        playSong: playSong,
        resumePlayer: resumePlayer,
        pausePlayer: pausePlayer,
        setCurrentSongDuration: setCurrentSongDuration,
        setElapsedTime: setElapsedTime
      };
    });
});