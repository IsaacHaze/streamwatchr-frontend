define(['app'], function(app){
  'use strict';
  app.directive('playButton', ['Player', 'IlpsLogging', function(Player, IlpsLogging) {
    return {
      restrict: 'E',
      scope: {
        images: '=',
        mbId: '=',
        videoId: '=',
        song: '=',
        artist: '='
      },
      templateUrl: '/partials/play-button.html',
      controller: function($scope, $element, $attrs, $transclude){
        $scope.state = Player.state;

        $scope.playToggle = function($event) {
          $event.preventDefault();
          $event.stopPropagation();

          if (Player.state.currentSong.video_id == $scope.videoId && Player.state.playState == 1) {
            Player.pausePlayer();

            IlpsLogging.logEvent('pause_song', {
              song: $scope.song,
              artist: $scope.artist,
              mbId: $scope.mbId,
              video_id: $scope.videoId,
              initiated_from: 'song_button',
              song_duration_s: Player.state.currentSong.duration_s,
              song_elapsed_s: Player.state.currentSong.elapsed_s
            });
          }
          else if (Player.state.currentSong.video_id == $scope.videoId && Player.state.playState == 2) {
            Player.resumePlayer();

            IlpsLogging.logEvent('resume_song', {
              song: $scope.song,
              artist: $scope.artist,
              mbId: $scope.mbId,
              video_id: $scope.videoId,
              initiated_from: 'song_button',
              song_duration_s: Player.state.currentSong.duration_s,
              song_elapsed_s: Player.state.currentSong.elapsed_s
            });
          }
          else {
            Player.playSong({
              song: $scope.song,
              artist: $scope.artist,
              mbId: $scope.mbId,
              images: $scope.images,
              video_id: $scope.videoId
            });

            IlpsLogging.logEvent('play_song', {
              song: $scope.song,
              artist: $scope.artist,
              mbId: $scope.mbId,
              video_id: $scope.videoId,
              initiated_from: 'song_button'
            });
          }
        };
      }
    };
  }]);
});