define([
  'angular',
  'angularUIBootstrap',
  'directives/play_button'
],
function(angular) {
  'use strict';

  return {
    template: '',
    controller: ['$window', '$stateParams', '$state', '$scope', '$modal', '$http', 'songInfo', 'IlpsLogging', function($window, $stateParams, $state, $scope, $modal, $http, songInfo, IlpsLogging) {
      var modalInstance = $modal.open({
        templateUrl: '/partials/song-details.html',
        windowTemplateUrl: '/partials/modal-window.html',
        controller: function($scope, $modalInstance) {
          IlpsLogging.logEvent('modal_open', {
            modal_type: 'song',
            song_id: $stateParams.songId
          });
          $scope.song = songInfo.data;
          $scope.loadingRecommendations = true;
          $scope.recommendations = {};
          $scope.playCounts = [{
            'key': 'Plays',
            'values': []
          }];
          $scope.loadingPlayCounts = true;

          $scope.countries = {"WF": "Wallis And Futuna Islands", "JP": "Japan", "JM": "Jamaica", "JO": "Jordan", "WS": "Samoa", "GW": "Guinea-Bissau", "GU": "Guam", "GT": "Guatemala", "GS": "South Georgia, South Sandwich Islands", "GR": "Greece", "GQ": "Equatorial Guinea", "GP": "Guadeloupe", "GY": "Guyana", "GF": "French Guiana", "GE": "Georgia", "GD": "Grenada", "GB": "United Kingdom", "GA": "Gabon", "GN": "Guinea", "GM": "Gambia", "GL": "Greenland", "GI": "Gibralter", "GH": "Ghana", "PR": "Puerto Rico", "PS": "Palestine", "PW": "Palau", "PT": "Portugal", "PY": "Paraguay", "PA": "Panama", "PF": "French Polynesia", "PG": "Papua New Guinea", "PE": "Peru", "PK": "Pakistan", "PH": "Philippines", "PN": "Pitcairn", "PL": "Poland", "PM": "St. Pierre And Miquelon", "ZM": "Zambia", "ZA": "South Africa", "ZW": "Zimbabwe", "ME": "Montenegro", "MD": "Moldava", "MG": "Madagascar", "MA": "Morocco", "MC": "Monaco", "MM": "Myanmar", "ML": "Mali", "MO": "Macao", "MN": "Mongolia", "MH": "Marshall Islands", "MK": "Macedonia", "MU": "Mauritius", "MT": "Malta", "MW": "Malawi", "MV": "Maldives", "MQ": "Martinique", "MP": "Northern Mariana Islands", "MS": "Montserrat", "MR": "Mauritania", "MY": "Malaysia", "MX": "Mexico", "MZ": "Mozambique", "FR": "France", "FX": "France Metropolitan", "FI": "Finland", "FJ": "Fiji", "FK": "Falkland Islands", "FM": "Micronesia", "FO": "Faroe Islands", "CK": "Cook Islands", "CI": "Cote D Ivoire", "CH": "Switzerland", "CO": "Colombia", "CN": "China", "CM": "Cameroon", "CL": "Chile", "CC": "Cocos Islands", "CA": "Canada", "CG": "Congo", "CF": "Central African Republic", "CD": "Congo", "CZ": "Czech Republic", "CY": "Cyprus", "CX": "Christmas Islands", "CR": "Costa Rica", "CV": "Cape Verde", "CU": "Cuba", "SZ": "Swaziland", "SY": "Syria", "SR": "Suriname", "SV": "El Salvador", "SU": "Soviet Union", "ST": "Sao Tome/Principe", "SK": "Slovakia", "SJ": "Svalbard And Jan Mayen", "SI": "Slovenia", "SH": "St. Helena", "SO": "Somalia", "SN": "Senegal", "SM": "San Marino", "SL": "Sierra Leone", "SC": "Seychelles", "SB": "Solomon Islands", "SA": "Saudi Arabia", "SG": "Singapore", "SE": "Sweden", "SD": "Sudan", "YE": "Yemen", "YT": "Mayotte", "LB": "Lebanon", "LC": "St. Lucia", "LA": "Laos", "LK": "Sri Lanka", "LI": "Liechtenstein", "LV": "Latvia", "LT": "Lithuania", "LU": "Luxembourg", "LR": "Liberia", "LS": "Lesotho", "LY": "Libyan Arab Jamahiriya", "VA": "Vatican City State", "VC": "St. Vincent & The Grenadines", "VE": "Venezuela", "VG": "British Virgin Islands", "IQ": "Iraq", "VI": "Virgin Islands (Usa)", "IS": "Iceland", "IR": "Iran", "IT": "Italy", "VN": "Viet Nam", "IM": "Isle Of Man", "IL": "Israel", "IO": "British Indian Ocean Terr", "IN": "India", "IE": "Ireland", "ID": "Indonesia", "BD": "Bangladesh", "BE": "Belgium", "BF": "Burkina Faso", "BG": "Bulgaria", "BA": "Bosnia And Herzegovina", "BB": "Barbados", "BM": "Bermuda", "BN": "Brunei Darussalam", "BO": "Bolivia", "BH": "Bahrain", "BI": "Burundi", "BJ": "Benin", "BT": "Bhutan", "BV": "Bouvet Island", "BW": "Botswana", "BR": "Brazil", "BS": "Bahamas", "BY": "Belarus", "BZ": "Belize", "RU": "Russia", "RW": "Rwanda", "RS": "Serbia", "RE": "Reunion", "RO": "Romania", "OM": "Oman", "HR": "Croatia", "HT": "Haiti", "HU": "Hungary", "HK": "Hong Kong", "HN": "Honduras", "HM": "Heard & Mcdonald Island", "EH": "Western Sahara", "EE": "Estonia", "EG": "Egypt", "EC": "Ecuador", "ET": "Ethiopia", "ES": "Spain", "UY": "Uruguay", "UZ": "Uzbekistan", "US": "United States", "UM": "United States", "UK": "United Kingdom", "UG": "Uganda", "UA": "Ukraine", "VU": "Vanuatu", "NI": "Nicaragua", "NL": "The Netherlands", "NO": "Norway", "NA": "Namibia", "NC": "New Caledonia", "NE": "Niger", "NF": "Norfolk Island", "NG": "Nigeria", "NZ": "New Zealand", "NP": "Nepal", "NR": "Nauru", "NU": "Niue", "KG": "Kyrgyzstan", "KE": "Kenya", "KI": "Kiribati", "KH": "Cambodia", "KN": "St. Kitts And Nevis", "KM": "Comoras", "KR": "South Korea", "KP": "North Korea", "KW": "Kuwait", "KZ": "Kazakhstan", "KY": "Cayman Islands", "DO": "Dominican Republic", "DM": "Dominica", "DJ": "Djibouti", "DK": "Denmark", "DE": "Germany", "DZ": "Algeria", "TZ": "Tanzania", "TV": "Tuvalu", "TW": "Taiwan", "TT": "Trinidad And Tobago", "TR": "Turkey", "TP": "East Timor", "TN": "Tunisia", "TO": "Tonga", "TM": "Turkmenistan", "TJ": "Tajikistan", "TK": "Tokelau", "TH": "Thailand", "TF": "French Southern Territories", "TG": "Togo", "TD": "Chad", "TC": "Turks And Calcos Islands", "AC": "Ascension Island", "AE": "United Arab Emirates", "AD": "Andorra", "AG": "Antigua And Barbuda", "AF": "Afghanistan", "AI": "Anguilla", "AM": "Armenia", "AL": "Albania", "AO": "Angola", "AN": "Netherlands Antilles", "AQ": "Antarctica", "AS": "Somoa,Gilbert,Ellice Islands", "AR": "Argentina", "AU": "Australia", "AT": "Austria", "AW": "Aruba", "AX": "Aland", "AZ": "Azerbaijan", "QA": "Qatar"};

          $scope.modalContentHeight = $window.innerHeight - 252;

          angular.element($window).bind('resize', function() {
            $scope.$apply(function() {
              $scope.modalContentHeight = $window.innerHeight - 252;
            });
          });

          $scope.chartXFunction = function() {
            return function(d) { return new Date(d[0]); };
          };

          $scope.chartXTickFormat = function() {
            return function(d) { return d3.time.format('%b %d')(new Date(d)); };
          };

          $scope.chartPathColor = function() {
            return function(d, i) { return '#40ac5a'; };
          };

          $scope.chartTooltipContent = function() {
            return function(key, x, y, e, graph) {
              console.log(key, x, y, e, graph);
              return  '<p>' +  y + ' plays at ' + x + '</p>';
            }
          }

          $scope.close = function() {
            IlpsLogging.logEvent('modal_close', {
              modal_type: 'song',
              song_id: $stateParams.songId
            });
            $modalInstance.dismiss('cancel');
          };

          var getPlayCounts = function() {
            var params = {};
            if('collection' in $state.current.data) {
              params.collection = $state.current.data.collection;
            }

            $http.get('/song-counts/' + songInfo.data.song_id, {
              params: params
            }).then(function(success){
              $scope.loadingPlayCounts = false;
              $scope.playCounts[0].values = success.data;
            });
          };

          var getRecommendedSongs = function() {
            var params = {};
            if('collection' in $state.current.data) {
              params.collection = $state.current.data.collection;
            }

            $http.get('/api/recommend/song/' + songInfo.data.song_id, {
              params: params
            }).then(function(success){
              $scope.loadingRecommendations = false;
              $scope.recommendations = success.data;
            });
          };

          getRecommendedSongs();
          getPlayCounts();
        }
      }).result.then(undefined, function(){
        // Return to the parent state when the modal is closed
        $state.go('^');

        // Ubind the window resize event
        angular.element($window).unbind('resize');
        $window.onresize = null;
      });
    }],
    resolve: {
      songInfo: function($state, $stateParams, $http) {
        var params = {};
        if('data' in this && 'collection' in this.data) {
          params.collection = this.data.collection;
        }

        return $http.get('/api/song-info/' + $stateParams.songId, {
          params: params
        });
      }
    }
  };
});