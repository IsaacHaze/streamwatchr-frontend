module.exports = function(grunt) {
  grunt.initConfig({
    requirejs: {
      js: {
        options: {
          uglify2: {
              mangle: false
          },
          optimize: 'uglify2',
          baseUrl: "public/js",
          mainConfigFile: 'public/js/main.js',
          include: ['almond.js'],
          name: 'main',
          preserveLicenseComments: false,
          out: 'public/build/main.js'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.registerTask('default', ['requirejs']);
};
