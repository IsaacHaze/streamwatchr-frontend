define(['angular'], function(angular) {
  'use strict';

  angular.module('streamwatchr.lyrics', ['streamwatchr.scrollticker'])
    .controller('LyricsController', ['$scope', 'CurrentlyPlaying', function($scope, CurrentlyPlaying) {
      $scope.stats = CurrentlyPlaying.stats;
    }]);
});