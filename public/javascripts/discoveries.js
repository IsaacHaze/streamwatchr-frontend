$( document ).ready(function() {
	var touchScreen = false;
	if ($('html').hasClass('touch')) { touchScreen = true; }

	// Discoveries calender
	if ($('#calendar').length != '') {	
		var date = new Date();
	    var d = date.getDate();
	    var m = date.getMonth();
	    var y = date.getFullYear();
	    $('#calendar').fullCalendar({
	        viewDisplay: function(view) {
	          var now = new Date();
	          now.setMonth(now.getMonth() - 11); 
	          var end = new Date();
	          end.setMonth(now.getMonth()-1);
	
	          var cal_date_string = view.start.getMonth()+'/'+view.start.getFullYear();
	          var cur_date_string = now.getMonth()+'/'+now.getFullYear();
	          var end_date_string = end.getMonth()+'/'+end.getFullYear();
	
	          if(cal_date_string == cur_date_string) { jQuery('.fc-button-prev').addClass("fc-state-disabled"); }
	          else { jQuery('.fc-button-prev').removeClass("fc-state-disabled"); }
	
	          if(end_date_string == cal_date_string) { jQuery('.fc-button-next').addClass("fc-state-disabled"); }
	          else { jQuery('.fc-button-next').removeClass("fc-state-disabled"); }
	        },
	        editable: false,
	        disableDragging: true,
	        eventClick: function(calEvent, jsEvent, view) {
	            
	        },
	        // height : 500,
	        contentHeight: 850,
	        buttonText: {
	            prev: '<div id="calendar-prev"><div class="button">previous month</div><div class="icon"></div></div>',
	            next: '<div id="calendar-next"><div class="button">next month</div><div class="icon"></div></div>',
	        },
	        events: [
	            {
	            	title: "",
	            	start: new Date(2013, 9, 4),
	                allDay: true,
	            	img: 'http://userserve-ak.last.fm/serve/252/82455937.png,http://userserve-ak.last.fm/serve/252/87463665.png',
	            	imgScale: 'hor, ver',
	                time: '16:00, 22:00',
	                target: 'youtube_id,youtube_id2' 
	            },
	            {
	            	title: "",
	            	start: new Date(2013, 9, 8),
	                allDay: true,
	            	img: 'http://userserve-ak.last.fm/serve/252/82455937.png',
	            	imgScale: 'ver',
	                time: '16:00',
	                target: 'youtube_id' 
	            },
	            {
	            	title: "",
	            	start: new Date(2013, 9, 2),
	                allDay: true,
	            	img: 'http://userserve-ak.last.fm/serve/252/44459735.png',
	            	imgScale: 'ver',
	                time: '16:00',
	                target: 'youtube_id' 
	            },
	            {
	            	title: "",
	            	start: new Date(2013, 9, 13),
	                allDay: true,
	            	img: 'http://userserve-ak.last.fm/serve/252/87463665.png',
	            	imgScale: 'ver',
	                time: '16:00',
	                target: 'youtube_id' 
	            },
	            {
	            	title: "",
	            	start: new Date(2013, 9, 18),
	                allDay: true,
	            	img: 'http://userserve-ak.last.fm/serve/252/44459735.png, http://userserve-ak.last.fm/serve/252/82455937.png,http://userserve-ak.last.fm/serve/252/87463665.png',
	            	imgScale: 'hor, hor, ver',
	                time: '08:00, 16:00, 22:00',
	                target: 'youtube_id,youtube_id2,youtube_id3' 
	            },
	        ],
	        eventRender: function(event, element) {
	        	element.find('.fc-event-inner').remove();
	        	var img = event.img.split(',');
	        	var imgScale = event.imgScale.split(',');
	        	var time = event.time.split(',');
	        	var target = event.target.split(',');
	        	
	        	var className = 'single';
	        	if (img.length == 2) {
	    			className = 'double';
	    		} else if (img.length == 3) {
	    			className = 'triple';
	    		} 
	    		var covers = '';
	        	$.each(target, function(i) {
        			covers +=	'<div class="cover cover_'+className+' cover_'+i+'" data-target="'+target[i]+'">';
	    			covers +=   '<img class="thumb '+imgScale[i]+'" src="'+img[i]+'" />';
	    			covers +=	'<div class="overlay"></div>';
	    			covers +=   '<div class="time">'+time[i]+'<small>u</small> UTC</div>';
	    			covers += 	'<div class="hover"><div class="play-btn">';
	    			covers += 	'<span><img src="img/play_icon.png" alt="play song"></span>'
	    			covers += 	'</div></div>'
	    			covers +=   '</div>';
		        })
	        	element.append(covers);
	        }
	    });

		var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var currentMonth = $('#calendar').fullCalendar('getDate').getMonth();
		$('#calendar-prev .button').html(monthNames[currentMonth-1]);
		$('#calendar-next .button').html(monthNames[currentMonth+1]);

		$('#calendar-prev').click(function() {
			setTimeout(function() {
				var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
				var currentMonth = $('#calendar').fullCalendar('getDate').getMonth();
				$('#calendar-next .button').html(monthNames[currentMonth+1]);
				if (currentMonth == 0) {
					$('#calendar-prev .button').html(monthNames[11]);
				} else {
					$('#calendar-prev .button').html(monthNames[currentMonth-1]);
				}
			}, 100);
		});

		$('#calendar-next').click(function() {
			setTimeout(function() {
				var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
				var currentMonth = $('#calendar').fullCalendar('getDate').getMonth();
				$('#calendar-prev .button').html(monthNames[currentMonth-1]);
				if (currentMonth == 11) {
					$('#calendar-next .button').html(monthNames[0]);
				} else {
					$('#calendar-next .button').html(monthNames[currentMonth+1]);
				}
			}, 100);
		});

		$('#calendar .fc-event .cover').click(function() {
			target = $(this).data('target');
			showPlayer(target);
		});
	}
});