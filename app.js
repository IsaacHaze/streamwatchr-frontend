var express = require('express');
var db = require('./models/db').db;
var dbrec = require('./models/dbrec').db;
var db_chordify = require('./models/db_chordify').db;
var http = require('http');
var path = require('path');
var async = require('async');

var app = express();

//=============================================================================
//		EXPRESS SETUP
//=============================================================================
app.configure( function() {
	// app.set('port', 53678);
	app.set('port', 2053);
	app.set('views', __dirname + '/views');
	app.set('view engine', 'jade');
	app.use(express.favicon( __dirname + '/public/images/favicon.ico'));
	app.use(express.logger('dev'));
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	// app.use(app.router);
	app.use(require('stylus').middleware(__dirname + '/public'));
	app.use(express.static(path.join(__dirname, 'public')));


	// Helper functions
	app.locals.numberWithCommas = function(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	};
	app.locals.capitalize = function(x) {
	    return x.charAt(0).toUpperCase() + x.slice(1);
	};
	app.locals.shortenName = function(x) {
		if (x.length > 25) {
	    	return x.substr(0,25) + "...";
	    }
	    return x;
	};
    app.use(redirectFarewell);

});


function redirectFarewell(req, res) {
    res.redirect("/streamwatchr-farewell/");
}


app.configure('development', function () {
	app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
	app.locals.pretty = true;
});

app.configure('production', function () {
	app.use(express.errorHandler());
});

var server = http.createServer(app);
server.listen(app.get('port'), function(){
		console.log("Express server listening on port " + app.get('port'));
});



//=============================================================================
//		SOCKET.IO SETUP
//=============================================================================
var io = require('socket.io').listen(server);
io.enable('browser client minification');
io.enable('browser client etag');
io.enable('browser client gzip');
io.set('log level', 1);										// reduce logging

io.set('transports', ['websocket',
	 'xhr-polling'
	, 'jsonp-polling'
	, 'htmlfile'

]);

//io.set('log level', 2); // very verbose without this
readAndSend();

//=============================================================================
//		CURRENTLY PLAYING
//=============================================================================
function readAndSend() {
	current = db.collection('currently_playing');
	latest_entry = current.findOne({},{'sort': {"$natural":-1} }, function (err, latest) {

		cursor = current.find({'_id': {'$gte': latest._id}},{'tailable': 1, 'awaitData': 1, 'sort':{'$natural':1}});
		cursor.each(function (err, song) {
			if(err) {
				console.log(err);
			} else {
				io.sockets.volatile.emit('song_play', song);
			}
		});
	});
}

//=============================================================================
//		ROUTING
//=============================================================================

app.get('/mu-9da5b06c-501d183a-be9897a2-197383db', function (req, res) {
	res.send('42');
});


app.get('/', function (req, res) {
	res.render('index');
});

app.get('/about', function (req, res) {
	res.render('about');
});

app.get('/api/overall-stats', function (req, res) {
	get_total_tweets(function (tweetCountErr, tweetCount) {
		get_unique_artists(function (uniqueArtistsErr, uniqueArtistsCount) {
			var results = {};

			if (!uniqueArtistsErr && uniqueArtistsCount) {
				results.unique_artists_count = uniqueArtistsCount;
			} else {
				results.unique_artists_count = null;
			}

			if (!tweetCountErr && tweetCount) {
				results.tweet_count = tweetCount;
			} else {
				results.tweet_count = null;
			}

			res.json(results);
		});
	});
});

app.get('/api/recently-played', function (req, res) {
	get_currently_start( function (err, songs) {
		res.json({songs: songs});
	});
});

app.get('/api/latest-lyrics', function (req, res) {
	get_latest_lyrics( function (err, lyrics) {
		res.json({ lyrics: lyrics});
	});
});

app.get('/api/song-info/:id', function (req, res) {
	var collection = 'song_popularity';
	if (req.param('collection') == 'lastday') {
		collection = 'song_stats_geo_daily';
	} else if (req.param('collection') == 'lastmonth') {
		collection = 'song_stats_geo_monthly';
	} else if (req.param('collection') == 'lastyear') {
		collection = 'song_stats_geo_yearly';
	} else if (req.param('collection') == 'lasthour') {
		collection = 'song_stats_geo_hourly';
	} else if (req.param('collection') == 'currently') {
		collection = 'currently_playing';
	}


	from_id_to_song(req.param('id'), collection, function(err, song) {
		console.log(song);
		get_song_count( song, function (err, count) {
			if (!err && count) {
				db.collection('song_stats_geo').find({'artists': song.artists, 'song': song.song, 'isRadio': false}, {}, {'sort': {'count': -1}, 'limit': 5}).toArray(function (err, countries) {
					if (!err) {
						db.collection('song_stats_overall').find({"artists.mbId": song.artists[0].mbId, "isRadio": false}, {'sort': {'count': -1}, 'limit': 5, 'hint': {'artists.mbId': 1, 'isRadio': 1, 'count': -1}}).toArray(function (err, songs) {
							if (!err) {
								db.collection('lyrics').find({"artists": song.artists, "song": song.song}, {'sort': {'created_at': -1}, 'limit': 5, 'hint': {'artists': 1, 'song': 1, 'created_at': -1}}).toArray(function (err, lyrics) {
									if (!err) {
										db.collection('artist_info').findOne({'mbId': song.artists[0].mbId}, {'hint': {'mbId':1} }, function (err, artist_info) {
											get_youtubeID(song.song, song.artists, function(err, yt_id) {
												res.json({'song_id': req.param('id'), 'collection': req.param('collection'), 'song_name': song, 'artist_info':artist_info, 'count':count, 'countries': countries, 'songs': songs, 'lyrics':lyrics, 'youtubeID': yt_id});
											});
										});
									}
								});
							}
						})
					}
				});
			}
		});
	});
});

app.get('/api/artist-info/:mbId', function (req, res) {
	get_artist_count( req.param('mbId'), function (err,count) {
		if (!err && count) {
			db.collection('artist_stats_geo').find({'mbId': req.param('mbId'), 'isRadio': false}, {}, {'sort': {'count': -1}, 'limit': 5}).toArray(function (err, countries) {
				if (!err) {
					db.collection('song_stats_overall').find({"artists.mbId": req.param('mbId'), "isRadio": false}, {'sort': {'count': -1}, 'limit': 5, 'hint': {'artists.mbId': 1, 'isRadio': 1, 'count': -1}}).toArray(function (err, songs) {
						if (!err) {
							db.collection('lyrics').find({"artists.mbId": req.param('mbId')}, {'sort': {'created_at': -1}, 'limit': 5, 'hint': {'artists.mbId': 1, 'created_at': -1}}).toArray(function (err, lyrics) {
								if (!err) {
									db.collection('lyrics_popularity').find({"artists.mbId": req.param('mbId')}, {'sort': {'count': -1}, 'limit': 5, 'hint': {'artists.mbId': 1, 'count': -1}}).toArray(function (err, lyrics_pop) {
										if (!err) {
											db.collection('artist_info').findOne({'mbId': req.param('mbId')}, {'hint': {'mbId':1} }, function (err, artist_info) {
												res.json({'artist_info':artist_info, 'count':count, 'countries': countries, 'songs': songs, 'lyrics':lyrics, 'lyrics_pop':lyrics_pop});
											});
										}
									});
								}
							});
						}
					})
				}
			});
		}
	});
});

app.get('/api/recommend/:entitytype/:id', function (req, res) {
	var now = new Date();
	var searchobj = {};
	if (req.param('entitytype') == 'artist') {
		searchobj = {type: "artist",
					"source.mbId": req.param('id'),
					}
					// seen: {"$gt": new Date(now - 4*3600*1000)},
		// searchobj.type = "artist";
		// searchobj."source.mbId" = req.param('id');

		get_recommendations(searchobj, function(err, recs) {
			if (!err && recs) {
				res.json({docs: recs});
			}
		});
	} else if (req.param('entitytype') == 'song') {
		var collection = 'song_popularity';
		if (req.param('collection') == 'currently') {
			collection = 'currently_playing';
		}

		searchobj.type = "song";
		from_id_to_song(req.param('id'), collection, function(err, song) {
			console.log(err);
			console.log(song);
			searchobj.source = {artists: song.artists, song: song.song}; // the keys need to be in this order
			// searchobj.seen = {"$gt": new Date(now - 4*3600*1000)};

			get_recommendations(searchobj, function(err, recs) {
				if (!err && recs) {
					async.forEach(recs, function(rec, callback) {
						get_youtubeID(rec.target.song, rec.target.artists, function(err, yt) {
							if (!err && yt) {
								rec.youtubeID = yt;
							}
							callback();
						});
					}, function (err) {
						res.json({docs: recs});
					});
				}
			})
		});
	} else {
		return;
	}

});

app.get('/api/popular/songs', function (req, res) {
	get_popular_songs( function (err, songs) {
		res.json({ docs: songs });
	});
});

app.get('/api/popular/artists', function (req, res) {
	get_popular_artists( function (err, artists) {
		res.json({ docs: artists });
	});
});

app.get('/api/off-the-beaten-track/artists', function (req, res) {
	get_periscope_artists( function (err, artists) {
		res.json({ docs: artists });
	});
});

app.get('/api/off-the-beaten-track/songs', function (req, res) {
	get_periscope_songs( function (err, songs) {
		res.json({ docs: songs });
	});
});

app.get('/api/song-stats-overall', function(req, res) {
	var sincedate = new Date(2013,1,1,0,0,0,0);
	var collection = "daily";

	if (req.param('trendline')) {
		var now = new Date();
		sincedate = new Date(now.getTime() - 24*3600*1000);
		sincedate.setUTCMinutes(0);
		sincedate.setUTCSeconds(0);
		sincedate.setUTCMilliseconds(0);

		collection = "hourly";
	}

	from_id_to_song(req.param('id'), 'song_popularity', function(err, song) {
		song_stats_overall(song, collection, sincedate, function(err, counts) {
			res.json(counts);
		});
	});
});

app.get('/api/artist-stats-overall', function(req, res) {
	var sincedate = new Date();
	sincedate.setUTCMinutes(0);
	sincedate.setUTCSeconds(0);
	sincedate.setUTCMilliseconds(0);
	sincedate.setUTCHours(0);
	sincedate.setUTCDate(sincedate.getUTCDate() - 24);

	artist_stats_overall(req.param('id'), 'daily', sincedate, function(err, counts) {
		res.json(counts);
	});
});


app.get('/api/chordify-url/:youtubeID', function(req, res) {
	var response_data = {
		youtubeId: req.param('youtubeID'),
		chordifyUrl: null
	};

	db_chordify.collection('youtube_to_chordify_cache').findOne({youtubeId: req.param('youtubeID')}, function(chordify_db_error, chordifyCachedDoc) {
		if (chordifyCachedDoc) {
			response_data.chordifyUrl = chordifyCachedDoc.chordifyUrl;
			res.json(response_data);
		} else {
			http.get('http://chordify.net/api/search?id=youtube:' + req.param('youtubeID'), function(chordifyResp) {
				chordifyResp.on('data', function(data) {

					try {
						data = JSON.parse(data);
					}
					catch (e) {
						res.status(500).json({error: e.message});
						return;
					}

					if ('song' in data && 'url' in data.song) {
						response_data.chordifyUrl = data.song.url;
						db_chordify.collection('youtube_to_chordify_cache').insert({
							youtubeId: response_data.youtubeId,
							chordifyUrl: response_data.chordifyUrl,
							createdAt: new Date()
						});
					}
					res.json(response_data);
				})
				.on('error', function(e) {
					res.status(500).json({error: e.message});
				});
			});
		}
	});
});

app.get('/home', function (req, res) {
	get_latest_lyrics( function (err, lyrics) {
		get_currently_start( function (err, songs) {
			get_total_tweets( function (err, count) {
				if (!err && count) {
					get_unique_artists( function (err2, count2) {
						if (!err2 && count2) {
							res.render('home', {start_songs : songs, 'unique_artists':count2,'total_tweets':count, 'lyrics':lyrics});
						} else
							res.render('home', {start_songs : songs, 'unique_artists':'(error)','total_tweets':count, 'lyrics':lyrics});
					});
				} else
					res.render('home', {start_songs : songs, 'unique_artists':'(error)','total_tweets':'(error)', 'lyrics':lyrics});
			});
		});
	});
});

app.get('/update-tweet-artist-count', function (req, res) {
	get_latest_lyrics( function (err, lyrics) {
		get_total_tweets( function (err, count) {
			if (!err && count) {
				get_unique_artists( function (err2, count2) {
					if (!err2 && count2) {
						res.json({unique_artists:count2,total_tweets:count, lyrics:lyrics});
					} else
						res.json({unique_artists:'(error)',total_tweets:count, lyrics:lyrics});
				});
			}
		});
	});
});

app.get('/popular/songs', function (req, res) {
	get_popular_songs( function (err, songs) {
		res.render('popular-songs', { docs: songs });
	});
});

app.get('/popular/artists', function (req, res) {
	get_popular_artists( function (err, artists) {
		res.render('popular-artists', { docs: artists });
	});
});

app.get('/off-the-beaten-track/artists', function (req, res) {
	get_periscope_artists( function (err, artists) {
		res.render('off-the-beaten-track-artists', { docs: artists });
	});
});

app.get('/off-the-beaten-track/songs', function (req, res) {
	get_periscope_songs( function (err, songs) {
		res.render('off-the-beaten-track-songs', { docs: songs });
	});
});

// app.get('/discover', function (req, res) {
// 	get_new_discoveries(function (err, discoveries) {
// 		res.render('discover', {docs: discoveries});
// 	});
// });

app.get('/world/:entitytype/:name/', function (req, res) {
	res.render('world', { timeunit: req.param('name'), entitytype: req.param('entitytype') });
});

app.get('/artist-counts/:id', function (req, res) {
	// We provide the mbId but we refer to it as id because
	// it needs the signature needs to be compatible with songs-counts
	var beginning = new Date(2013,1,1);
	artist_stats_overall(req.param('id'), 'daily', beginning, function (err, data) {
		res.json(data);
	});
});

app.get('/song-counts/:id', function (req, res) {
	var collection = 'song_popularity';
	if (req.param('collection') == 'lastday') {
		collection = 'song_stats_geo_daily';
	} else if (req.param('collection') == 'lastmonth') {
		collection = 'song_stats_geo_monthly';
	} else if (req.param('collection') == 'lastyear') {
		collection = 'song_stats_geo_yearly';
	} else if (req.param('collection') == 'lasthour') {
		collection = 'song_stats_geo_hourly';
	} else if (req.param('collection') == 'currently') {
		collection = 'currently_playing';
	}


	// We provide the _id but we refer to it as id because
	// it needs the signature needs to be compatible with artist-counts
	var beginning = new Date(2013,1,1);
	from_id_to_song(req.param('id'), collection, function(err, song) {
		song_stats_overall(song, 'daily', beginning, function (err, data) {
			res.json(data);
		});
	});
});

app.get('/recommend/:entitytype/:id', function (req, res) {
	var now = new Date();
	var searchobj = {};
	if (req.param('entitytype') == 'artist') {
		searchobj = {type: "artist",
					"source.mbId": req.param('id'),
					}
					// seen: {"$gt": new Date(now - 4*3600*1000)},
		// searchobj.type = "artist";
		// searchobj."source.mbId" = req.param('id');

		get_recommendations(searchobj, function(err, recs) {
			if (!err && recs) {
				res.render('box-recommendations', {docs: recs});
			}
		});
	} else if (req.param('entitytype') == 'song') {
		var collection = 'song_popularity';
		if (req.param('collection') == 'currently') {
			collection = 'currently_playing';
		}

		searchobj.type = "song";
		from_id_to_song(req.param('id'), collection, function(err, song) {
			console.log(err);
			console.log(song);
			searchobj.source = {artists: song.artists, song: song.song}; // the keys need to be in this order
			// searchobj.seen = {"$gt": new Date(now - 4*3600*1000)};

			get_recommendations(searchobj, function(err, recs) {
				if (!err && recs) {
					async.forEach(recs, function(rec, callback) {
						get_youtubeID(rec.target.song, rec.target.artists, function(err, yt) {
							if (!err && yt) {
								rec.youtubeID = yt;
							}
							callback();
						});
					}, function (err) {
						res.render('box-recommendations', {docs: recs});
					});
				}
			})
		});
	} else {
		return;
	}

});

app.get('/recommend-radio', function (req, res) {
	var now = new Date();
	var searchobj = {type: "song"};

	searchobj.source = {artists: req.param('artists'), song: req.param('song')}; // the keys need to be in this order
	// searchobj.seen = {"$gt": new Date(now - 4*3600*1000)};

	get_recommendations(searchobj, function(err, recs) {
		if (!err && recs) {
			async.forEach(recs, function(rec, callback) {
				get_youtubeID(rec.target.song, rec.target.artists, function(err, yt) {
					if (!err && yt) {
						rec.youtubeID = yt;

						console.log(rec.target.artists[0].mbId);

						get_lastfm_image(rec.target.artists[0].mbId, function(err, lfm) {
							if (!err && lfm) {
								rec.image = lfm;
							}
							callback();
						});
					} else {
						callback();
					}
				});
			}, function (err) {
				// console.log(recs);
				res.json({docs: recs});
			});
		}
	})

});

app.get('/world/:entitytype/:name/c-:country', function(req, res) {
	var thisdate = new Date();

	var collection = 'hourly';
	thisdate.setUTCMinutes(0);
	thisdate.setUTCSeconds(0);
	thisdate.setUTCMilliseconds(0);

	var humandescription = "this hour";
	var humandescription1 = "This hour's";
	var humandescription2 = "hour";

	if (req.param('name') == 'lastday') {
		collection = 'daily';
		thisdate.setUTCHours(0);

		humandescription = "today";
		humandescription1 = "Today's";
		humandescription2 = "day";
	} else if (req.param('name') == 'lastmonth') {
		collection = 'monthly';
		thisdate.setUTCHours(0);
		thisdate.setUTCDate(1);

		humandescription = "this month";
		humandescription1 = "This month's";
		humandescription2 = "month";
	} else if (req.param('name') == 'lastyear') {
		collection = 'yearly';
		thisdate.setUTCHours(0);
		thisdate.setUTCDate(1);
		thisdate.setUTCMonth(0);

		humandescription = "this year";
		humandescription1 = "This year's";
		humandescription2 = "year";
	}

	if (req.param('country') == 'data') {
		get_world_stats(thisdate, collection, function (err, data) {
			res.json(data);
		});
	} else {
		if (req.param('entitytype') == 'song') {
			db.collection('song_stats_geo_'+collection)
				.find({"country": req.param('country'), "date": thisdate, "isRadio": false},
							{},
							{'sort': {'count': -1}, 'limit': 10, 'hint': {'country':1, 'date':-1, 'isRadio':1, 'count':-1} })
				.toArray(function (err, songs) {
					if (!err && songs) {
						async.forEach(songs, function(song, callback) {
							get_lastfm_image(song.artists[0]['mbId'], function(err, lfm) {
								if (!err && lfm) {
									song.image = lfm;

									get_youtubeID(song.song, song.artists, function(err, yt) {
										if (!err && yt) {
											song.youtubeID = yt;
										}
										callback();
									});
								} else { callback(); }
							});
						}, function(err) {
							res.render('world-country-song', {docs: songs, humandescription: humandescription, humandescription1: humandescription1, humandescription2: humandescription2, country: req.param('country'), url: req.url, period: req.param('name'), entitytype: req.param('entitytype')});
						});
					}
				});
		} else if (req.param('entitytype') == 'artist') {
			db.collection('artist_stats_geo_'+collection)
				.find({"country": req.param('country'), "date": thisdate, "isRadio": false},
							{},
							{'sort': {'count': -1},
							 'limit': 10,
							 'hint': {'country':1, 'date':-1, 'isRadio':1, 'count':-1} })
				.toArray(function (err, artists) {
					if (!err && artists) {
						async.forEach(artists, function(artist, callback) {
							get_lastfm_info(artist.mbId, function(err, lfm) {
								if (!err && lfm) {
									artist.image = lfm.lastfm_info.artist.image;
									artist.name = lfm.lastfm_info.artist.name;
									callback();
								} else { callback(); }
							});
						}, function(err) {
							res.render('world-country-artist', {docs: artists, humandescription: humandescription, humandescription1: humandescription1, humandescription2: humandescription2, country: req.param('country'), url: req.url, period: req.param('name'), entitytype: req.param('entitytype')});
						});
					}
				});
		} else {
			console.log("I don't know where I'm! "+req.param('entitytype'))
		}

	}
});

app.get('/world/:entitytype/:name/stats.json', function(req, res) {
	// Query parameters:
	// id			 The MongoDb song id from song_stats_geo_* tables
	// country	The country for which we want the stats
	thisdate = new Date();
	collection = 'hourly';
	thisdate.setUTCMinutes(0);
	thisdate.setUTCSeconds(0);
	thisdate.setUTCMilliseconds(0);
	sincedate = thisdate
	sincedate.setUTCHours(thisdate.getUTCHours() - 24);
	if (req.param('name') == 'lastday') {
		collection = 'daily';
		thisdate.setUTCHours(0);
		sincedate.setUTCDate(thisdate.getUTCDate() - 30);
	} else if (req.param('name') == 'lastmonth') {
		collection = 'monthly';
		thisdate.setUTCHours(0);
		thisdate.setUTCDate(1);
		sincedate.setUTCMonth(thisdate.getUTCMonth() - 12);
	} else if (req.param('name') == 'lastyear') {
		collection = 'yearly';
		thisdate.setUTCHours(0);
		thisdate.setUTCDate(1);
		thisdate.setUTCMonth(0);
		sincedate.setUTCFullYear(thisdate.getUTCFullYear() - 10);
	}
	console.log(req.param("entitytype"));
	console.log(collection);
	if (req.param('entitytype') == 'song') {
		from_id_to_song(req.param('id'), 'song_stats_geo_'+collection, function(err, song) {
			song_stats_geo(song, req.param('country'), collection, sincedate, function(err, counts) {
				res.json(counts);
			});
		});
	} else if (req.param('entitytype') == 'artist') {
		artist_stats_geo(req.param('id'), req.param('country'), collection, sincedate, function(err, counts) {
			res.json(counts);
		});
	}
});

app.get('/song-stats-overall.json', function(req, res) {
	var sincedate = new Date(2013,1,1,0,0,0,0);
	var collection = "daily";

	if (req.param('trendline')) {
		var now = new Date();
		sincedate = new Date(now.getTime() - 24*3600*1000);
		sincedate.setUTCMinutes(0);
		sincedate.setUTCSeconds(0);
		sincedate.setUTCMilliseconds(0);

		collection = "hourly";
	}

	from_id_to_song(req.param('id'), 'song_popularity', function(err, song) {
		song_stats_overall(song, collection, sincedate, function(err, counts) {
			res.json(counts);
		});
	});
});

app.get('/artist-stats-overall.json', function(req, res) {
	var sincedate = new Date();
	sincedate.setUTCMinutes(0);
	sincedate.setUTCSeconds(0);
	sincedate.setUTCMilliseconds(0);
	sincedate.setUTCHours(0);
	sincedate.setUTCDate(sincedate.getUTCDate() - 24);

	artist_stats_overall(req.param('id'), 'daily', sincedate, function(err, counts) {
		res.json(counts);
	});
});

app.get('/info/:mbId', function (req, res) {
	db.collection('artist_info').findOne({'mbId': req.param('mbId')}, {'hint': {'mbId':1} }, function (err, artist_info) {
		res.json(artist_info);
	});
})

app.get('/artist-info/:mbId', function (req, res) {
	get_artist_count( req.param('mbId'), function (err,count) {
		if (!err && count) {
			db.collection('artist_stats_geo').find({'mbId': req.param('mbId'), 'isRadio': false}, {}, {'sort': {'count': -1}, 'limit': 5}).toArray(function (err, countries) {
				if (!err) {
					db.collection('song_stats_overall').find({"artists.mbId": req.param('mbId'), "isRadio": false}, {'sort': {'count': -1}, 'limit': 5, 'hint': {'artists.mbId': 1, 'isRadio': 1, 'count': -1}}).toArray(function (err, songs) {
						if (!err) {
							db.collection('lyrics').find({"artists.mbId": req.param('mbId')}, {'sort': {'created_at': -1}, 'limit': 5, 'hint': {'artists.mbId': 1, 'created_at': -1}}).toArray(function (err, lyrics) {
								if (!err) {
									db.collection('lyrics_popularity').find({"artists.mbId": req.param('mbId')}, {'sort': {'count': -1}, 'limit': 5, 'hint': {'artists.mbId': 1, 'count': -1}}).toArray(function (err, lyrics_pop) {
										if (!err) {
											db.collection('artist_info').findOne({'mbId': req.param('mbId')}, {'hint': {'mbId':1} }, function (err, artist_info) {
												res.render('artist-info', {'artist_info':artist_info, 'count':count, 'countries': countries, 'songs': songs, 'lyrics':lyrics, 'lyrics_pop':lyrics_pop});
											});
										}
									});
								}
							});
						}
					})
				}
			});
		}
	});
});

app.get('/song-info/:id', function (req, res) {
	var collection = 'song_popularity';
	if (req.param('collection') == 'lastday') {
		collection = 'song_stats_geo_daily';
	} else if (req.param('collection') == 'lastmonth') {
		collection = 'song_stats_geo_monthly';
	} else if (req.param('collection') == 'lastyear') {
		collection = 'song_stats_geo_yearly';
	} else if (req.param('collection') == 'lasthour') {
		collection = 'song_stats_geo_hourly';
	} else if (req.param('collection') == 'currently') {
		collection = 'currently_playing';
	}


	from_id_to_song(req.param('id'), collection, function(err, song) {
		get_song_count( song, function (err, count) {
			if (!err && count) {
				db.collection('song_stats_geo').find({'artists': song.artists, 'song': song.song, 'isRadio': false}, {}, {'sort': {'count': -1}, 'limit': 5}).toArray(function (err, countries) {
					if (!err) {
						db.collection('song_stats_overall').find({"artists.mbId": song.artists[0].mbId, "isRadio": false}, {'sort': {'count': -1}, 'limit': 5, 'hint': {'artists.mbId': 1, 'isRadio': 1, 'count': -1}}).toArray(function (err, songs) {
							if (!err) {
								db.collection('lyrics').find({"artists": song.artists, "song": song.song}, {'sort': {'created_at': -1}, 'limit': 5, 'hint': {'artists': 1, 'song': 1, 'created_at': -1}}).toArray(function (err, lyrics) {
									if (!err) {
										console.log(lyrics);
										db.collection('artist_info').findOne({'mbId': song.artists[0].mbId}, {'hint': {'mbId':1} }, function (err, artist_info) {
											res.render('song-info', {'song_id': req.param('id'), 'collection': req.param('collection'), 'song_name': song, 'artist_info':artist_info, 'count':count, 'countries': countries, 'songs': songs, 'lyrics':lyrics});
										});
									}
								});
							}
						})
					}
				});
			}
		});
	});
});


app.get('/about', function (req, res) {
	get_total_tweets( function (err, count) {
		if (!err && count) {
			get_unique_artists( function (err2, count2) {
				if (!err2 && count2) {
					res.render('about', {'unique_artists':count2,'total_tweets':count});
				} else
					res.render('about', {'unique_artists':'(error)','total_tweets':count});
			});
		} else
			res.render('about', {'unique_artists':'(error)','total_tweets':'(error)'});
	});
});


var get_world_stats = function (thisdate, collection, callback) {
	console.log(thisdate)
	console.log(collection)

	db.collection('country_stats_'+collection).find({"date": thisdate, "isRadio": false}, {'hint': {'date':-1, 'isRadio':1} })
	.toArray( function (err, rec) {
		if (!err && rec) {
			async.forEach(rec, function(r, callback) {
				callback();
			}, function (err) {
				console.log(rec)
				callback(undefined, rec)
			});
		}
	});
}

var song_stats_geo = function (song, country, collection, thisdate, callback) {
	db.collection('song_stats_geo_'+collection)
	 .find(
		 {'artists': song.artists, 'song': song.song, 'country': country, 'isRadio': false, 'date': {'$gte': thisdate}},
		 {'count':1},
		 {'sort': {'date': 1}, 'hint': {'artists':1, 'song':1, 'country':1, 'isRadio':1, 'date':-1} })
	 .toArray(function(err, results) {
		 var counts = [];
		 async.forEach(results, function(item, callback){
			 counts.push(item.count);
			 callback();
		 }, function(err) {
			 callback(undefined, counts);
		 });
	 });
}


var artist_stats_geo = function (mbId, country, collection, thisdate, callback) {
	console.log(mbId);
	db.collection('artist_stats_geo_'+collection)
	 .find(
		 {'mbId': mbId, 'country': country, 'isRadio': false, 'date': {'$gte': thisdate}},
		 {'count':1},
		 {'sort': {'date': 1}, 'hint': {'mbId':1, 'country':1, 'isRadio':1, 'date':-1} })
	 .toArray(function(err, results) {
		 var counts = [];
		 console.log(results);
		 async.forEach(results, function(item, callback){
			 counts.push(item.count);
			 callback();
		 }, function(err) {
			 callback(undefined, counts);
		 });
	 });
}

var song_stats_overall = function (song, collection, thisdate, callback) {
	db.collection('song_stats_overall_'+collection)
	 .find(
		 {'artists': song.artists, 'song': song.song, 'isRadio': false, 'date': {'$gte': thisdate}},
		 {'date':1, 'count':1},
		 {'sort': {'date': 1}, 'hint': {'artists':1, 'song':1, 'isRadio':1, 'date':-1} })
	 .toArray(function(err, results) {
		 var counts = [];
		 async.forEach(results, function(item, callback){
			 counts.push([item.date, item.count]);
			 callback();
		 }, function(err) {
			 callback(undefined, counts);
		 });
	 });
}

var artist_stats_overall = function (mbId, collection, thisdate, callback) {
	db.collection('artist_stats_overall_'+collection)
	 .find(
		 {'mbId': mbId, 'isRadio': false, 'date': {'$gte': thisdate}},
		 {'date':1, 'count':1},
		 {'sort': {'date': 1}, 'hint' : {'mbId':1, 'isRadio':1, 'date':-1} })
	 .toArray(function(err, results) {
		 var counts = [];
		 async.forEach(results, function(item, callback){
			 counts.push([item.date, item.count]);
			 callback();
		 }, function(err) {
			  callback(undefined, counts);
		 });
	 });
}

var get_youtubeID = function (song, artists, callback) {
	db.collection('song_info').findOne( {'artists':artists, 'song':song} , {'videoId':1} , {'hint' : {'artists':1, 'song':1, 'videoId':1} } , function (err, id) {
		if (err) {
			throw err;
		}
		if (id) {
			callback(undefined, id['videoId']);
		} else {
			callback(err, id);
		}
	});
};


var get_lastfm_image = function (artist, callback) {
  db.collection('artist_info').findOne({'mbId':artist}, {}, {'hint': {'mbId':1} }, function (err, info) {
    if (err) {
      throw err;
    }
    if (info) {
      callback(undefined, info.lastfm_info.artist.image);
    } else {
      callback(err, info);
    }
  });
};

var get_lastfm_info = function (artist, callback) {
  db.collection('artist_info').findOne({'mbId':artist}, {}, {'hint': {'mbId':1} }, function (err, info) {
    if (err) {
      throw err;
    }
    if (info) {
      callback(undefined, info);
    } else {
      callback(err, info);
    }
  });
};


var from_id_to_song = function (_id, collection, callback) {
	object = {};
	allowed_collections = ['currently_playing','song_popularity', 'song_stats_geo_daily', 'song_stats_geo_hourly', 'song_stats_geo_monthly', 'song_stats_geo_yearly', 'song_stats_overall_daily', 'song_stats_overall_hourly', 'song_stats_overall_monthly', 'song_stats_overall_yearly'];

	if (allowed_collections.indexOf(collection) > -1) {
		db.collection(collection).find({'_id':db.ObjectID.createFromHexString(_id)}).toArray( function (err, songs) {
			if (!err && songs) {
				async.forEach(songs, function (song, callback) {
					object['song'] = song['song'];
					object['artists'] = song['artists'];
					object['video_id'] = song['video_id'];
					console.log(object);
					callback();
				}, function (err) {
					callback(undefined, object);
				});
			}
		});
	}
};

var from_id_to_artist = function (_id, collection, callback) {
	object = {};
	allowed_collections = ['artist_popularity'];

	if (allowed_collections.indexOf(collection) > -1) {
		db.collection(collection).find({'_id':db.ObjectID.createFromHexString(_id)}).toArray( function (err, artists) {
			if (!err && artists) {
				async.forEach(artists, function (artist, callback) {
					object['artists'] = song['artists'];
					callback();
				}, function (err) {
					callback(undefined, object);
				});
			}
		});
	}
};

var get_popular_songs = function (callback) {
	var timedamping = 0.693;
	var timeunit = 10*60;

	var startrange = new Date();
	var endrange = new Date(startrange.getTime()-60*10*1000);
	db.collection('song_popularity')
	 .find({"ts": {"$gt": endrange}, "tempdf": {"$gt": 1}}, {'artists':1,'song':1,'tempdf':1,'ts':1}, {'sort': {'tempdf':-1}, 'limit':40, 'hint':{'ts':-1, 'tempdf':-1} })
	 .toArray( function (err, songs) {
		if (!err && songs) {
			async.forEach(songs, function (song, callback) {

				var factor = Math.exp(-1 * timedamping * Math.abs((startrange - song['ts'].getTime())/1000) / timeunit);
				var tempdf_new = song["tempdf"] * factor + 1;
				song.tempdf = tempdf_new;

				get_lastfm_image(song.artists[0]['mbId'], function(err, lfm) {
					if (!err && lfm) {
						song.image = lfm;

						get_youtubeID(song.song, song.artists, function(err, yt) {
							if (!err && yt) {
								song.youtubeID = yt;
							}
							callback();
						});
					} else { callback(); }
				});
			}, function (err) {
				songs.sort( function (a,b) { return (a.tempdf < b.tempdf) ? 1 : ((b.tempdf < a.tempdf) ? -1 : 0); } );
				callback(undefined, songs);
			});
		}
	});
};

var get_popular_artists = function (callback) {
	var timedamping = 0.693;
	var timeunit = 10*60;

	startrange = new Date();
	endrange = new Date(startrange.getTime()-60*10*1000);
	db.collection('artist_popularity').find({"ts": {"$gt": endrange}, "tempdf": {"$gt": 1}}, {'artist':1,'tempdf':1, 'mbId':1, 'ts':1}, {'sort': {'tempdf':-1}, 'limit':40, 'hint':{'ts': -1, 'tempdf': -1} }).toArray( function (err, artists) {
		if (!err && artists) {
			async.forEach(artists, function (artist, callback) {

				var factor = Math.exp(-1 * timedamping * Math.abs((startrange.getTime() - artist['ts'].getTime())/1000) / timeunit);
				var tempdf_new = artist["tempdf"] * factor + 1;
				artist.tempdf = tempdf_new;

				get_lastfm_image(artist.mbId, function(err, lfm) {
					if (!err && lfm) {
						artist.image = lfm;
						callback();
					} else { callback(); }
				});
			}, function (err) {
				artists.sort( function (a,b) { return (a.tempdf < b.tempdf) ? 1 : ((b.tempdf < a.tempdf) ? -1 : 0); } );
				callback(undefined, artists);
			});
		}
	});
};

var get_total_tweets = function (callback) {
	db.collection('archive').count( function (err, count) {
		callback(undefined, count);
	});
};

var get_unique_artists = function (callback) {
	db.collection('artist_stats_overall').count( function (err, count) {
		callback(undefined, count);
	});
};

var get_periscope_artists = function (callback) {
	startrange = new Date();
	endrange = new Date(startrange.getTime()-10*60*1000);
	db.collection('randommatrix').find(
		{'type': 'artist', 'lastsum': {'$gt': 150}, 'lastseen': {'$gt': endrange}},
		{'entity': 1},
		{'sort': {'stdrate': 1, 'laststd': -1}, 'limit': 20, 'hint':{ 'type':1, 'lastsum':1, 'lastseen':-1, 'stdrate':1, 'laststd':-1} }).toArray( function (err, artists) {
		if (!err && artists) {
			async.forEach(artists, function (artist, callback) {
				db.collection('artist_info').findOne({'mbId':artist.entity}, function (err, info) {
					if (!err && info) {
						artist.artist_image = info.lastfm_info.artist.image;
						artist.name = info.lastfm_info.artist.name;
						artist.mbId = info.mbId;
					}
					callback();
				});
			}, function (err) {
				callback(undefined, artists);
			});
		}
	});
};

var get_periscope_songs = function (callback) {
	startrange = new Date();
	endrange = new Date(startrange.getTime()-10*60*1000);
	lastdatebin = new Date(startrange.getTime()-6*3600*1000);
	lastdatebin.setMinutes(0);
	lastdatebin.setSeconds(0);
	lastdatebin.setMilliseconds(0);
	db.collection('randommatrix').find(
		{type: 'song', lastsum: {$gte: 50}, lastseen: {$gt: endrange}},
		{entity: 1, stdrate: 1, laststd: 1},
		{sort: {stdrate: 1, laststd: -1},
		 limit: 100,
		 hint: {type:1, lastsum:1, lastseen:-1, stdrate:1, laststd:-1}
	 	}).toArray( function (err, songs) {
		if (!err && songs) {
			async.filter(songs, function (song, filter_callback) {
				db.collection('song_popularity').findOne(
					{song:song.entity},
					{sort: {tempdf: -1},
					 hint: {song:1, tempdf:-1} },
					function (err, most_popular_song) {
					if (!err && most_popular_song) {
						song.song_popularity_id = most_popular_song._id;
						song.mbId = most_popular_song.artists;
						console.log("Candidate: "+song.entity);

						// Get the hourly popularity of this song
						db.collection('song_stats_overall_hourly').aggregate(
							{$match: {song:song.entity,
									 artists:most_popular_song.artists,
									 date: {$gte: lastdatebin}}},
							{$group: {_id: "$song", counts: {$sum: "$count"}}},
							function (err, checkedsong) {
							if (!err && checkedsong && checkedsong[0] && checkedsong[0].counts >= 5) {
								console.log({song:song.entity,
									 artists:most_popular_song.artists,
									 date: {$gte: lastdatebin},
									 count: {$gte: 5}});

								console.log("  ... count: " + checkedsong[0].counts);
								db.collection('artist_info').findOne(
									{mbId: song.mbId[0].mbId},
									{hint: {mbId:1} },
									function (err, artist_info) {
									if (!err && artist_info) {
										song.artist_image = artist_info.lastfm_info.artist.image;
										song.artist_name = artist_info.lastfm_info.artist.name;
										song.artist_mbId = artist_info.mbId;

										console.log(song.artist_image);

										get_youtubeID(song.entity, song.mbId, function(err, yt) {
											if (!err && yt) {
												song.youtubeID = yt;
											}
											filter_callback(true);

										});
									} else {
										filter_callback(false);
									}
								});
							} else {
								console.log("song_stats_..."+err, checkedsong, lastdatebin);

								filter_callback(false);
							}
						});

					} else {
						filter_callback(false);
					}
				});
			}, function (songs) {
					callback(undefined, songs);
			});
		} else {
			callback(err, null);
		}
	});
};

var get_new_discoveries = function (callback) {
	db.collection('discovered').find({}, {}, {'sort': {'date': -1}, 'limit': 50, 'hint': {'date':-1} }).toArray(function(err, songs) {
		if (!err && songs) {
			async.forEach(songs, function(song, callback) {
        get_lastfm_image(song.artists[0]['mbId'], function(err, lfm) {
          if (!err && lfm) {
            song.image = lfm;

            get_youtubeID(song.song, song.artists, function(err, yt) {
              if (!err && yt) {
                song.youtubeID = yt;
              }
              callback();
            });
          } else { callback(); }
        });
			}, function (err) {
				callback(undefined, songs);
			});
		}
	});
}


var get_currently_start = function (callback) {
	db.collection('currently_playing').find({'images' : {'$ne': null}, 'images.3.#text' : {'$ne' : ''}}, {}, {'sort': {"$natural":-1}, 'limit': 100 }).toArray(function (err, songs) {
		if (!err && songs) {
			console.log(songs);
			callback(undefined, songs);
		}
	});
};

var get_latest_lyrics = function (callback) {
	db.collection('lyrics').find({},{},{'sort':{'created_at':-1}, 'limit':7}).toArray(function (err, lyrics) {
		if(!err && lyrics) {
			callback(undefined, lyrics);
		}
	});
};

var get_artist_count = function (mbId, callback) {
	db.collection('artist_stats_overall').findOne({'mbId':mbId, 'isRadio':false}, {'hint': {'mbId':1,'isRadio':1} }, function (err, count) {
		if (!err && count) {
			callback(undefined, count['count']);
		}
	});
};

var get_song_count = function (song, callback) {
	db.collection('song_stats_overall').findOne(
		{"artists": song.artists, 'isRadio':false, 'song':song.song},
		{'hint': {'artists': 1, 'isRadio':1, 'song':1,} }, function (err, count) {
		if (!err && count) {
			callback(undefined, count['count']);
		}
	});
};

var get_half_life = function (searchobj, callback ) {
	if (searchobj.type === "song") {
		db.collection("randommatrix").findOne(
			{type: searchobj.type, entity: searchobj.source.song}, function (err, rec) {
				if (!err && rec) {
					// Update the mean
					var now = new Date();
					console.log(now);
					console.log(rec.lastpos);

					console.log("HALF LIFE TIME DIFF: " + (now.getTime() - rec.lastpos.getTime())/1000);
					var x_i = (now.getTime() - rec.lastpos.getTime()) / (1000 * rec.lastmean);
					var lastmean = rec.lastmean + (x_i - rec.lastmean) / (rec.lastsum+1);

					callback(undefined, lastmean);
				} else {
					callback(err, null);
				}
			})

	} else {
		db.collection("randommatrix").findOne(
			{type: searchobj.type, entity: searchobj["source.mbId"]}, function (err, rec) {
				if (!err && rec) {
					// Update the mean
					var now = new Date();

					var x_i = (now.getTime() - rec.lastpos.getTime()) / (1000 * rec.lastmean);
					var lastmean = rec.lastmean + (x_i - rec.lastmean) / (rec.lastsum+1);

					callback(undefined, lastmean);
				} else {
					callback(err, null);
				}
			})
	}
};

var half_life = 1;
var get_recommendations = function (searchobj, gencallback) {
	var now = new Date();
	console.log(now);

	console.log("Before firing:");
	console.log(searchobj);

	// Get half-life of the song
	get_half_life(searchobj, function (err, this_half_life) {
		if (!err && this_half_life) {
			console.log("Half-life: " + this_half_life);

			dbrec.collection("graph").find(searchobj,
										{seen: true, target: true, weight: true},
										{limit: 100, hint: {type: 1, source: 1, seen: -1, target:1, weight: 1}})
					.toArray(function (err, targets) {
						if (!err && targets) {
							// console.log(targets);
							async.forEach(targets, function (target, callback) {
								target.edgeweight = 0.;
								target.egoedgescount = 0;
								target.curweight = target.weight * Math.exp(-0.69315 * (now.getTime() - target.seen.getTime()) / (1000 * this_half_life));
								// console.log(target);
								console.log("Time difference in seconds: " + (now.getTime() - target.seen.getTime())/1000);
								get_half_life(searchobj, function (err, target_half_life) {
									if (!err && target_half_life) {
										console.log("Target half-life: " + target_half_life);

										dbrec.collection("graph").find({type: searchobj.type, target: target.target},
																	{seen: true, weight: true},
																	{limit: 100, hint: {type: 1, target: 1, seen: -1, weight: 1}})
												.toArray(function (err, egoedges) {
													if (err) { callback(); }
													if (egoedges) {
														async.forEach(egoedges, function (egoedge, mycallback) {
															target.edgeweight += egoedge.weight * Math.exp(-0.69315 * (now.getTime() - egoedge.seen.getTime()) / (1000 * target_half_life));
															target.egoedgescount += 1;

															mycallback();
														}, function(err) {
															target.normweight = target.curweight / target.edgeweight;
															if (target.edgeweight == 0. || target.egoedgescount < 3) {
																target.normweight = 0.;
															}
															callback();
														});
													} else {
														callback();
													}
												});
									} else {
										callback();
									}
								});
							}, function (err) {
								// console.log(targets);
								async.filter(targets, function(target, mycallback) {
									if (target.normweight == 0) {
										mycallback(false);
									} else {
										mycallback(true);
									}
								}, function(res) {
									// Sort targets by weight
									res.sort(function(a, b) {
										return b.normweight - a.normweight;
									});
									gencallback(err, res.slice(0,5));
								});
							});
						}
					});
		} else {
			gencallback(err, null);
		}
	});
};
