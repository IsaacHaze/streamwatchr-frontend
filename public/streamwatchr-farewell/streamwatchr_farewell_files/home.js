define([
  'app',
  'directives/cover_swap'
],
function(app) {
  'use strict';

  return {
    parent: 'layout',
    url: '/',
    templateUrl: 'partials/home.html',
    resolve: {
      recentlyPlayedSongs: function(CurrentlyPlaying) {
        return CurrentlyPlaying.getRecentlyPlayedSongs();
      }
    },
    controller: ['$scope', 'Pagetitle', 'CurrentlyPlaying', 'ImagePreload', 'Player', 'recentlyPlayedSongs', 'IlpsLogging', function($scope, Pagetitle, CurrentlyPlaying, ImagePreload, Player, recentlyPlayedSongs, IlpsLogging) {
      Pagetitle.set('Currently playing');

      // The number of songs to display
      var maxDisplayedSongs = 66;

      $scope.stats = CurrentlyPlaying.stats;
      $scope.songs = [];

      // Populate the grid with the last played songs
      recentlyPlayedSongs.data.songs.map(updateCurrentlyPlaying);

      // Update the grid when a new song comes in
      $scope.$watch('stats.lastPlayedSong', updateCurrentlyPlaying);

      $scope.play = function(song, $event){
        if ($event.stopPropagation) $event.stopPropagation();
        if ($event.preventDefault) $event.preventDefault();
        
        $event.cancelBubble = true;
        $event.returnValue = false;
        
        Player.playSong(song);
      };

      var hoverSongStartTime = null;
      $scope.startSongHoverEvent = function() {
        hoverSongStartTime = new Date();
      };

      $scope.stopSongHoverEvent = function (song_position) {
        var hoverDuration = new Date() - hoverSongStartTime;

        if (hoverDuration >= 400) {
          IlpsLogging.logEvent('song_hover', {
            tweet_id: this.song.tweet_id,
            song: this.song.song,
            artist: this.song.artist,
            hover_duration_ms: hoverDuration,
            song_position: song_position,
            total_songs: $scope.songs.length
          });
        }
        hoverSongStartTime = null;
      };

      function updateCurrentlyPlaying(song_data) {
        if (!song_data.images || song_data.images.length < 5 || song_data.images[3]['#text'] === "" || song_data.images[4]['#text'] === "") {
          return;
        }

        // Only add the song to the page when the cover image is preloaded
        ImagePreload.preload(song_data.images[3]['#text']).then(function(data) {
          // Pass the image's actual dimensions (used to determine styling)
          song_data.images[3].width = data.width;
          song_data.images[3].height = data.height;

          if (data.width > data.height) {
            song_data.images[3].imgClass = 'ver';
          } else {
            song_data.images[3].imgClass = 'hor';
          }

          // Just append to the array of songs if we haven't reached the max
          // allowed songs, otherwise select a random song on the page to replace
          if ($scope.songs.length < maxDisplayedSongs) {
            $scope.songs.push(song_data);
          } else {
            var newSongPosition = Math.floor(Math.random() * maxDisplayedSongs);
            $scope.songs[newSongPosition] = song_data;
          }
        });
      }
    }]
  };
});