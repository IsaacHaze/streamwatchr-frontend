// Keep track of all AJAX requests
$.xhrPool = [];
$.xhrPool.abortAll = function() {
    $(this).each(function(idx, jqXHR) {
        jqXHR.abort();
    });
    $.xhrPool.length = 0;
};

$.ajaxSetup({
	timeout: 10000,
    beforeSend: function(jqXHR) {
        $.xhrPool.push(jqXHR);
    },
    always: function(jqXHR) {
        var index = $.inArray(jqXHR, $.xhrPool);
        if (index > -1) {
            $.xhrPool.splice(index, 1);
        }
    }
});

// helper functions
String.prototype.capitalize = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function shortenName(x) {
	if (x.length > 25) {
		return x.substr(0,25) + "...";
	}
	return x;
};

// Polling tweet and artist counts and lyrics
var timer;
function doPoll() {
	$.getJSON('/update-tweet-artist-count')
		.done(function (data) {
			// Logic for syncing the tweet counts
			// is not yet implemented because we do
			// not know how to stop the io.socket
			// and flip the tiles. We could flip random
			// tiles but these will be different from
			// user to user.
			
			// get counts for Authors
			var artists = parseInt($('.total_artists').text().replace(",",""));
			var diff = data.unique_artists - artists;
			if (diff > 0) {
				incr(artists, diff, '.total_artists');
			}
			
		})
		.always(function () {
			timer = setTimeout(doPoll, 30000); // Poll every 30 seconds
		})
}

function incr(initialcount, difference, element) {
	var curval = initialcount
	var updater = setTimeout(function() {
		$(element).html(numberWithCommas(++curval));
		if (initialcount == (curval+difference)) {
			clearTimeout(updater);
		}
	}, Math.random()*250);
}

// Slider effect
$.fn.slideTo = function(data) {
    var width = parseInt($('#container').css('width'));
    var transfer = $('<div class="transfer"></div>').css({ 'width': (2 * width) + 'px' });
    var current = $('<div class="current"></div>').css({ 'width': width + 'px', 'left': '0', 'float': 'left' }).html($('#container').html());
    var next = $('<div class="next"></div>').css({ 'width': width + 'px', 'left': width + 'px', 'float': 'left' }).html(data);
    transfer.append(current).append(next);
    $('#container').html('').append(transfer);
    transfer.animate({ 'margin-left': '-' + width + 'px' }, 300, function () {
        $('#container').html(data);
    });
}

// Bouncing effect for long song titles + artists
jQuery.fn.bounce = function(options) {
    options = $.extend({
        speed: 60 // pixels per second
    }, options);

    this.each(function() {
        var element = $(this);
        var maxLeft = element.width() - element.parent().width();
    
        var slideLeft = function() {
            element.animate({left: -maxLeft}, {
                duration: maxLeft / options.speed * 1000,
                complete:slideRight
            });
        }
        var slideRight = function() {
            element.animate({left: 0}, {
                duration: maxLeft / options.speed * 1000,
                complete: slideLeft
            });
        }
    
        if (maxLeft > 0) {
            slideLeft();
        }
    });
	console.log("in bounce");
    return this;
}
jQuery.fn.unbounce = function() {
    this.stop().animate({left: 0}, "fast");
    return this;
};



var currentlyPlayingSong = {}; // holds the info of the song being
							   // played in the player.

// Charts
var mapcountries = {"AD": "Andorra", "AE": "United Arab Emirates", "AF": "Afghanistan", "AG": "Antigua and Barbuda",  "AI": "Anguilla", "AL": "Albania",  "AM": "Armenia",  "AO": "Angola", "AQ": "Antarctica", "AR": "Argentina",  "AS": "American Samoa", "AT": "Austria", "AU": "Australia", "AW": "Aruba", "AX": "Åland Islands", "AZ": "Azerbaijan",  "BA": "Bosnia and Herzegovina", "BB": "Barbados", "BD": "Bangladesh", "BE": "Belgium", "BF": "Burkina Faso", "BG": "Bulgaria", "BH": "Bahrain", "BI": "Burundi", "BJ": "Benin", "BL": "Saint Barthélemy", "BM": "Bermuda", "BN": "Brunei Darussalam", "BO": "Bolivia", "BQ": "Bonaire", "BR": "Brazil", "BS": "Bahamas", "BT": "Bhutan", "BV": "Bouvet Island", "BW": "Botswana", "BY": "Belarus", "BZ": "Belize", "CA": "Canada", "CC": "Cocos (Keeling) Islands", "CD": "Democratic Republic of the Congo", "CF": "Central African Republic", "CG": "Congo", "CH": "Switzerland", "CI": "Côte d'Ivoire", "CK": "Cook Islands", "CL": "Chile", "CM": "Cameroon", "CN": "China", "CO": "Colombia", "CR": "Costa Rica", "CU": "Cuba", "CV": "Cape Verde", "CW": "Curaçao", "CX": "Christmas Island", "CY": "Cyprus", "CZ": "Czech Republic", "DE": "Germany", "DJ": "Djibouti", "DK": "Denmark", "DM": "Dominica", "DO": "Dominican Republic", "DZ": "Algeria", "EC": "Ecuador", "EE": "Estonia", "EG": "Egypt", "EH": "Western Sahara", "ER": "Eritrea", "ES": "Spain", "ET": "Ethiopia", "FI": "Finland", "FJ": "Fiji", "FK": "Falkland Islands (Malvinas)", "FM": "Micronesia", "FO": "Faroe Islands", "FR": "France", "GA": "Gabon", "GB": "United Kingdom", "GD": "Grenada", "GE": "Georgia", "GF": "French Guiana", "GG": "Guernsey", "GH": "Ghana", "GI": "Gibraltar", "GL": "Greenland", "GM": "Gambia", "GN": "Guinea", "GP": "Guadeloupe", "GQ": "Equatorial Guinea", "GR": "Greece", "GS": "South Georgia and the South Sandwich Islands", "GT": "Guatemala", "GU": "Guam", "GW": "Guinea-Bissau", "GY": "Guyana", "HK": "Hong Kong", "HM": "Heard Island and McDonald Islands", "HN": "Honduras", "HR": "Croatia", "HT": "Haiti", "HU": "Hungary", "ID": "Indonesia", "IE": "Ireland", "IL": "Israel", "IM": "Isle of Man", "IN": "India", "IO": "British Indian Ocean Territory", "IQ": "Iraq", "IR": "Iran", "IS": "Iceland", "IT": "Italy", "JE": "Jersey", "JM": "Jamaica", "JO": "Jordan", "JP": "Japan", "KE": "Kenya", "KG": "Kyrgyzstan", "KH": "Cambodia", "KI": "Kiribati", "KM": "Comoros", "KN": "Saint Kitts and Nevis", "KP": "North Korea", "KR": "South Korea", "KW": "Kuwait", "KY": "Cayman Islands", "KZ": "Kazakhstan", "LA": "Laos", "LB": "Lebanon", "LC": "Saint Lucia", "LI": "Liechtenstein", "LK": "Sri Lanka", "LR": "Liberia", "LS": "Lesotho", "LT": "Lithuania", "LU": "Luxembourg", "LV": "Latvia", "LY": "Libya", "MA": "Morocco", "MC": "Monaco", "MD": "Moldova", "ME": "Montenegro", "MF": "Saint Martin (French part)", "MG": "Madagascar", "MH": "Marshall Islands", "MK": "Macedonia", "ML": "Mali", "MM": "Myanmar", "MN": "Mongolia", "MO": "Macao", "MP": "Northern Mariana Islands", "MQ": "Martinique", "MR": "Mauritania", "MS": "Montserrat", "MT": "Malta", "MU": "Mauritius", "MV": "Maldives", "MW": "Malawi", "MX": "Mexico", "MY": "Malaysia", "MZ": "Mozambique", "NA": "Namibia", "NC": "New Caledonia", "NE": "Niger", "NF": "Norfolk Island", "NG": "Nigeria", "NI": "Nicaragua", "NL": "Netherlands", "NO": "Norway", "NP": "Nepal", "NR": "Nauru", "NU": "Niue", "NZ": "New Zealand", "OM": "Oman", "PA": "Panama", "PE": "Peru", "PF": "French Polynesia", "PG": "Papua New Guinea", "PH": "Philippines", "PK": "Pakistan", "PL": "Poland", "PM": "Saint Pierre and Miquelon", "PN": "Pitcairn", "PR": "Puerto Rico", "PS": "Palestine", "PT": "Portugal", "PW": "Palau", "PY": "Paraguay", "QA": "Qatar", "RE": "Réunion", "RO": "Romania", "RS": "Serbia", "RU": "Russia", "RW": "Rwanda", "SA": "Saudi Arabia", "SB": "Solomon Islands", "SC": "Seychelles", "SD": "Sudan", "SE": "Sweden", "SG": "Singapore", "SH": "Saint Helena, Ascension and Tristan da Cunha", "SI": "Slovenia", "SJ": "Svalbard and Jan Mayen", "SK": "Slovakia", "SL": "Sierra Leone", "SM": "San Marino", "SN": "Senegal", "SO": "Somalia", "SR": "Suriname", "SS": "South Sudan", "ST": "Sao Tome and Principe", "SV": "El Salvador", "SX": "Sint Maarten (Dutch part)", "SY": "Syria", "SZ": "Swaziland", "TC": "Turks and Caicos Islands", "TD": "Chad", "TF": "French Southern Territories", "TG": "Togo", "TH": "Thailand", "TJ": "Tajikistan", "TK": "Tokelau", "TL": "Timor-Leste", "TM": "Turkmenistan", "TN": "Tunisia", "TO": "Tonga", "TR": "Turkey", "TT": "Trinidad and Tobago", "TV": "Tuvalu", "TW": "Taiwan", "TZ": "Tanzania", "UA": "Ukraine", "UG": "Uganda", "UM": "United States Minor Outlying Islands", "US": "United States", "UY": "Uruguay", "UZ": "Uzbekistan", "VA": "Holy See (Vatican City State)", "VC": "Saint Vincent and the Grenadines", "VE": "Venezuela", "VG": "Virgin Islands, British", "VI": "Virgin Islands, U.S.", "VN": "Vietnam", "VU": "Vanuatu", "WF": "Wallis and Futuna", "WS": "Samoa", "YE": "Yemen", "YT": "Mayotte", "ZA": "South Africa", "ZM": "Zambia", "ZW": "Zimbabwe", "XK": "Kossovo"};

function drawChart(entitytype, entityid) {
	d3.json('/'+entitytype+'-counts/'+entityid)
	.on("beforesend", function() {$("#chart_div_plot #msg").html("Loading chart ..."); })
	.on("error", function(error) {$("#chart_div_plot #msg").html("Loading the chart takes longer than usual. Please try again."); })
	.get(function(error, counts) {
		$("#chart_div_plot #msg").html("");
		nv.addGraph(function() {
		  var chart = nv.models.lineChart()
		                .margin({right: 70})  //Adjust chart margins to give the x-axis some breathing room.
		                .useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
		                .transitionDuration(350)  //how fast do you want the lines to transition?
		                // .showLegend(true)       //Show the legend, allowing users to turn on/off line series.
		                .showYAxis(true)        //Show the y-axis
		                .showXAxis(true)        //Show the x-axis
						.rightAlignYAxis(true)  // Align Y-axis to the right
						.x(function(d) {return new Date(d[0])})
						.y(function(d) {return d[1]})
		  ;
 
		  chart.xAxis     //Chart x-axis settings
		      .tickFormat(function(d) {
            return d3.time.format('%b %d')(new Date(d))
          });
 
		  chart.yAxis     //Chart y-axis settings
		      .tickFormat(d3.format('s'));

		  var myData = [{key: "Plays", values: counts}];
		
		  d3.select('#chart_div_plot svg')    //Select the <svg> element you want to render the chart in.   
		      .datum(myData)         //Populate the <svg> element with chart data...
		      .call(chart);          //Finally, render the chart!
 
		  //Update the chart when window resizes.
		  nv.utils.windowResize(function() { chart.update() });
		  return chart;
		});
	});
};

function drawRegionsMap(period) {
	var options = {
		backgroundColor: 'transparent',
		colorAxis: {colors: ['#f490ed']},
		legend : false,
		tooltip: {
			textStyle: {color: '#FF0000'}, 
			showColorCode: true,
			trigger: true,
		},
	};

	$.getJSON(period+'c-data', function(data) {
		// setup the new map and its variables
		var map = new google.visualization.DataTable();
			map.addColumn('string', 'Country');
			map.addColumn('number', 'Plays');
			map.addRows(data.length);  // length gives us the number of results in our returned data
		// now we need to build the map data, loop over each result
		$.each(data, function(i,v) {
			// set the values for both the name and the population
			map.setValue(i, 0, mapcountries[v.country]);
			map.setValue(i, 1, v.count);
		});
		// finally, create the map!
		var worldchart = new google.visualization.GeoChart(document.getElementById('map_div'));
		google.visualization.events.addListener(worldchart, "regionClick", function (eventData) {
			var region = eventData["region"];
			openLightbox(period+'c-'+region, 'popular');
		});
		worldchart.draw(map, options);
	});
};


// Screen sizes
var resizeMap = 0;
var grid = 0;
var currentGrid = 0;
var setGridSize = function() {
	if ($(window).width() >= 1464) {
		grid = 'wide';
	} else {
		grid = 'small';
	}
	if (grid != currentGrid) {
		var gridClasses = ["small", "wide"];
		$.each(gridClasses, function(i, v){
	       $('body').removeClass(v);
	    })
		var newClass = grid;
		$('body').addClass(newClass);
		currentGrid = grid;
		if ($('#chart_div').length != '') {
			resizeMap();
		}
	}
}

$('#setGrid').ready(function() {
	setGridSize();	
});


// player 
function showPlayer(data) {
	if (!playerready) {
		return;
	}
	
	currentlyPlayingSong = data;
	console.log(currentlyPlayingSong);
	
	// get the recommendation for this song
	$("#player_balk .time").show();
	$("#player_balk .time").html("Fetching songs that people listen to next ...");
    $.ajax({
      url: '/recommend-radio',
	  type: 'GET',
	  data: {song: unescape(currentlyPlayingSong.song), artists: currentlyPlayingSong.mbId},
	  context: $("#player_balk .time"),
      timeout: 20000,
    })
    .done(function (thisdata, textStatus, jqXHR) {
      if (thisdata && thisdata != undefined && !isEmpty(thisdata.docs)) {
		  for (var i=0;i<thisdata.docs.length;i++) {
			  if (!thisdata.docs[i].youtubeID) {
				  continue;
			  }
			  
			  console.log(thisdata.docs[i]);
	          currentlyPlayingSong = {
	  				youtube:thisdata.docs[i].youtubeID, 
	  				song:escape(thisdata.docs[i].target.song), 
	  				artist:escape(thisdata.docs[i].target.artists[0].name), 
	  				mbid:thisdata.docs[i].target.artists[0].mbId, 
	  				thumb:thisdata.docs[i].image[2]['#text'], 
	  				mbId:thisdata.docs[i].target.artists
	  			};
		  
			  $(this).html('Next: <a href="#" id="playnext">'+thisdata.docs[i].target.song.capitalize() + ' - ' + thisdata.docs[i].target.artists[0].name+'</a>');
			  $("#playnext").click(function(e) {
				  e.preventDefault();
				  showPlayer(currentlyPlayingSong);
			  });
			  
			  break;
		  }
      } else {
		currentlyPlayingSong = null;
        $(this).html("No songs to suggest after this one. Tap onto a new one.");
      }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
      if (textStatus == "timeout") {
        $(this).html("No songs to suggest after this one. Tap onto a new one.");
      } else {
        $(this).html("No songs to suggest after this one. Tap onto a new one.");
      }
	  currentlyPlayingSong = null;
    });
	
	
	// $("#player .time").hide(); // hide the closing info
	$("#player_balk .song .title").html(unescape(data.song));
	$("#player_balk .song .artist").html(unescape(data.artist));
	typlayer.loadVideoById(data.youtube);
	$("#player .inner #thumb").data('target','/artist-info/'+data.mbid);
	$img = $("#player .inner #thumb").find('img');
	$img.attr('src',data.thumb);
	$img.imagesLoaded().progress( function(instance, image) {
			if (image.img.width > image.img.height) {
				image.img.className = 'ver';
			} else {
				image.img.className = 'hor';
			}
		});	

	$("#player").addClass('active').animate({'bottom' : 64, 'opacity' : 1}, 500);
	$("#player .inner #play-btn").find('img').attr('src','/images/pauze_icon.png');
	
	// Update the twitter button data
	twitter_text = '#np '+ unescape(data.artist) + ' - ' + unescape(data.song).capitalize();
	$('a.twitter').data({"text":twitter_text, "href":"http://youtu.be/"+data.youtube});
}

var showYoutube = function() {
	$('#player #show_youtube').addClass('active');
	$('#player #youtube').stop().animate({'top' : '-407'}, 500);
}

var hideYoutube = function() {
	$('#player #show_youtube').removeClass('active');
	$('#player #youtube').stop().animate({'top' : '0'}, 500);
}

// Create YouTube player(s) after the API code downloads.
var yt = document.createElement('script');
yt.src = 'https://www.youtube.com/player_api';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(yt, s);

var typlayer;
function onYouTubeIframeAPIReady() {
    typlayer = new YT.Player('typlayer', {
		height: 342,
		width: 600,
		playerVars: {
			autoplay: 1,
			controls: 0,
			fs: 1,
			showinfo: 0,
			iv_load_policy: 3,
			enablejsapi: 1},
    	events: {
    		'onReady' : onPlayerReady,
			'onStateChange': onPlayerStateChange
    	}
    });
}

var playerready = false;
function onPlayerReady(event) {
	// placeholder for any functionality on
	// the player becomes ready for interaction
	playerready = true;
}

var closingtimer;
var closingtimercountdown;
function onPlayerStateChange(event) {
	
	var song = $("#player_balk .song .title").html();
	var artist = $("#player_balk .song .artist").html();
	
	_gaq.push(['_trackEvent', 'Videos', 'Status:'+event.data, song+' - '+artist]);
	
	if (event.data == YT.PlayerState.ENDED && currentlyPlayingSong !== null) {
		showPlayer(currentlyPlayingSong);
	}
	
	if(event.data == YT.PlayerState.ENDED || event.data == YT.PlayerState.PAUSED) {
		$("#player .inner #play-btn").find('img').attr('src','/images/play_icon.png');
		
		 
		// hide player after 10 secs the video has ended
		// $("#player_balk .time").show();
		// $('span#countdown').html(15+' ');
		// closingtimercountdown = setInterval(function () {
		// 	var cd = $('span#countdown');
		// 	cd.html((parseInt(cd.html())-1)+' ');
		// }, 1000);
		// closingtimer = setTimeout( function () {
		// 	hideYoutube();
		// 	$("#player").animate({'bottom' : -64, 'opacity' : 0}, 500).removeClass('active');
		// 	}, 15000);

	} else if(event.data == YT.PlayerState.PLAYING) {
		$("#player .inner #play-btn").find('img').attr('src','/images/pauze_icon.png');
		
		// Reset player counter
		// $("#player_balk .time").hide();
		// $('span#countdown').html(15+' ');
		// if (closingtimer) {
		// 	clearTimeout(closingtimer);
		// }
		// if (closingtimercountdown) {
		// 	clearInterval(closingtimercountdown);
		// }
	}
	
}


// Currently playing
function display_song(totalCovers, obj) {
    if (!obj.images || obj.images.length < 5 || obj.images[3]['#text'] == "" || obj.images[4]['#text'] == "") {
        return;
    }
    
    tweets += 1;
    $('div.total_songs').html(tweets.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
	
    var cover = Math.floor(Math.random() * (totalCovers+1));
    $cover = $('#grid > ul > li').eq(cover);
    var index = 3;
    var $cover_size = $cover.attr('class');
    if ($cover_size == "big") {
        index = 4;
    }

    var $subcover = null;
    var $altcover = null;

    if ($cover.find('.card.flipped').length == '') {
        $cover.find('.card').addClass('flipped');

        $subcover = $cover.find('.back');
        $altcover = $cover.find('.front');

    } else {
        $cover.find('.card').removeClass('flipped');

        $subcover = $cover.find('.front');
        $altcover = $cover.find('.back');
    }

    var $thumb = $subcover.find('.thumb');
    $thumb.html('<img class="hor" src="'+obj.images[index]['#text']+'" alt="'+obj.artist+'"/>');

    var $img = $thumb.find('img');
    var pic_real_width, pic_real_height;
    $("<img/>").attr("src", $img.attr("src")).load(function() {
        pic_real_width = this.width;
        pic_real_height = this.height;

        if (pic_real_width > pic_real_height) {
            $img.attr('class','ver');
        }
    });

    $subcover.find('.title').html(shortenName(obj.song));
    $subcover.find('.artist').html(shortenName(obj.artists[0].name));
    var $play_button = $subcover.find('img.play_youtube');
	
	
    if(obj.video_id !== null || obj.video_id !== undefined || obj.video_id !== "") {
        $play_button.data('song',{
				youtube:obj.video_id, 
				song:escape(obj.song), 
				artist:escape(obj.artist), 
				mbid:obj.artists[0].mbId, 
				thumb:obj.images[2]['#text'], 
				mbId:obj.mbId
			});
			// console.log($play_button.data());
        $subcover.find('div.play-btn').show();
    } else {
        $play_button.data('song',{});
    	$subcover.find('div.play-btn').hide();
    }
    $subcover.data('target','/song-info/'+obj._id+'?collection=currently');


    setTimeout(function() {
        $altcover.removeClass('active');
        $subcover.addClass('active');
    }, 250);
	
}

function isEmpty(d) {
	for (var i in d) {return false;}
	return true;
}

function loadEventListeners() {
	// Open player or artist info
	$("#grid ul li .card .face, ul#popular_now li.row").click(function(e) {
		var $img = $(this).find('img.yt-play'); 
		if (e.target.className.indexOf('yt-play') !== -1) {
			showPlayer($img.data('song'));
		} else {
			// if there is a YouTubeID attach a click listener
			if (!isEmpty($img.data('song'))) {
				$("#info-play").show();
				$("#info-play").click(function(e){
					showPlayer($img.data('song'));
				});
			} else {
				// Otherwise, hide the play button on the info page
				$("#info-play").hide();
			}
			openLightbox($(this).data('target'), 'artist_bio');
		}
	});

	// Close lightbox
	$('#lb_bg, #lb .lb_outer_close, #lb #lb_balk #lb_close, #footer, #sitewide').click(function(e) {
		if ($('#lb').hasClass('active')) {
			closeLightbox();
		}
	});
	

	// placeholder
	if(!Modernizr.input.placeholder){
		// REMEMBER TO UNCOMMENT THE Modernizr SCRIPT 
		// IN template.jade
        $('[placeholder]').focus(function() {
          var input = $(this);
          if (input.val() == input.attr('placeholder')) {
            input.val('');
            input.removeClass('placeholder');
          }
        }).blur(function() {
          var input = $(this);
          if (input.val() == '' || input.val() == input.attr('placeholder')) {
            input.addClass('placeholder');
            input.val(input.attr('placeholder'));
          }
        }).blur();
        $('[placeholder]').parents('form').submit(function() {
          $(this).find('[placeholder]').each(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
              input.val('');
            }
          })
        });
	}
}

function loadWorldEventListeners() {
	var period = $('ul#switch li.active ul li.active a').data('type');
	drawRegionsMap(period);
	
	// Click on Artist or Song
	$("ul#switch > li > a").click(function(e) {
		e.preventDefault();
		
		// remove .active from Artist and Song
		$(this).parent().siblings().removeClass('active');
		// add .active to where I clicked
		$(this).parent().addClass('active');
		
		var period = $(this).parent().find('ul li.active a').data('type');
		drawRegionsMap(period);
	});
	
	// Click on different periods (Today, This Month, This Year, ...)
	$('ul#switch > li > ul > li > a').click(function(e) {
		e.preventDefault();
		
		// remove .active from periods
		$('ul#switch > li > ul > li').removeClass('active');
		// add .active to the corresponding period in the other entity type
		//   first get the index of the li we clicked in this entity type
		myindex = $('ul#switch > li.active > ul > li').index($(this).parent());
		// set the corresponding index to .active
		$('ul#switch > li > ul').each(function() {
			$($(this).children('li').get(myindex)).addClass('active');
		});
		var period = $(this).data('type');
		drawRegionsMap(period);
	});
}

function loadGridEventListeners(socket) {
    // Homepage ticker
    $(".twitter_balk ul").liScroll();

    var artist = null;
    var count = null;

    var totalCovers = 0;
	totalCovers = $("#grid > ul > li").length;
    // $("#grid ul li.small, #grid ul li.big").each(function(i) {
    //     totalCovers = i;
    // });

    socket.on('JSON', function(obj) {
        display_song(totalCovers, obj);
    });


	$('#grid .hor').imagesLoaded().progress( function(instance, image) {
		if (image.img.width > image.img.height) {
			image.img.className = 'ver';
		} else {
			image.img.className = 'hor';
		}
	});
}

function loadOneTimeEventListeners() {
	if ($('html').hasClass('touch')) { touchScreen = true; }

	// Search
	$("#sitewide .search_btn").hover(function() {
		$(this).addClass('active');
	}, function() {
		$(this).removeClass('active');
	});


	// Set up Player + controls
	$("#player .inner #thumb").click(function() {
		if (!$('#lb').hasClass('active')) {
			var target = $(this).data('target');
			openLightbox(target, 'artist_bio');
		} else {
			closeLightbox('no_bg');
			var target = $(this).data('target');
			setTimeout(function() {
			   openLightbox(target, 'artist_bio');
			}, 1000);
		}
	});

	$("#player .inner #play-btn").click(function() {
		if(typlayer.getPlayerState() == YT.PlayerState.PLAYING) {
			typlayer.pauseVideo();
		} else if(typlayer.getPlayerState() == YT.PlayerState.ENDED || typlayer.getPlayerState() == YT.PlayerState.PAUSED) {
			typlayer.playVideo();
		}
	});

	$("#player_balk .song").hover(
	    function(){$(this).bounce();},
	    function(){$(this).unbounce();}
	);

	$("#player .inner #show_youtube").click(function() {
		if (!$(this).hasClass('active')) {
			showYoutube();
		} else {
			hideYoutube();
		}
	});

	$('a.twitter').click(function(e){
		e.preventDefault();
		var data = $(this).data();

		var loc = data.href;
		var title = encodeURIComponent(data.text);
		var via = "streamwatchr";
		var hashtags = "swtchr";

		window.open('http://twitter.com/share?url=' + loc + '&text=' + title + '&via='  + via + '&hashtags=' + hashtags, 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
	});

	// hide title hover
	$('[title]').each(function(){
	    $(this).data('original-title', $(this).attr('title'));
	}).hover(
	    function () { 
	        $(this).attr('title','')
	    }, function () { 
	        $(this).attr('title',$(this).data('original-title'))
	});
	
	// Cancel closing timer
	$('a#closingin10').click(function(e) {
		// console.log('inside..');
		e.preventDefault();
		
		if (closingtimer) {
			clearTimeout(closingtimer);
		}
		if (closingtimercountdown) {
			clearInterval(closingtimercountdown);
		}
		$('span#countdown').html(15+' ');
		$(this).parent().hide();
	})
}


var socket = null;
$( document ).ready(function() {
	loadOneTimeEventListeners();

	var History = window.History; // Note: We are using a capital H instead of a lower h
	History.replaceState({href:"/home",title:"Currently playing",item:"currently"}, "Currently playing", "");
	var State = History.getState();
		
	$(window)
		.resize(function(){
		   setGridSize();
		   resizeLightbox();
		})
		.bind('statechange', function () {
			var State = History.getState();
			loadContent(State.data);
			// console.log("State changed");
			// console.log(State);
		});
	
	// check the URL in the first entry
	// and load the right content
	if (State.url.indexOf('now') !== -1) {
		loadContent({href:"/home",title:"Currently playing",item:"currently"});
	} else if (State.url.indexOf('hot') !== -1) {
		loadContent({href:"/popular/songs",title:"Popular on the planet, now!",item:"popular"});
	} else if (State.url.indexOf('unexpected') !== -1) {
		loadContent({href:"/off-the-beaten-track/songs",title:"Unexpected music on the planet",item:"unexpected"});
	} else if (State.url.indexOf('about') !== -1) {
		loadContent({href:"/about",title:"About Streamwatchr",item:"about"});
	} else {
		loadContent({href:"/home",title:"Currently playing",item:"currently"});
	}
	
	// top menu
	$('ul.menu li a').click(function(e) {
		e.preventDefault();
		
		$this = $(this);
		
		// Clicked on a menu item, clear all pending AJAX requests
		$.xhrPool.abortAll();
		
		// Find the new target
		var data = $this.data('target');
		History.pushState(data, data.title, $this.attr('href'));
	});
});

function loadContent(data) {
	// console.log(data);
	
	// Clean up stuff (disconnect socket, remove bindings)
	if (socket) {
		socket.disconnect();
	}
	$('#container').empty();
	// Clear polling
	if (timer) {
		clearTimeout(timer);
	}
	// Load new page
	$.ajax({
		url: data.href,
		context: $( "#container" ),
		beforeSend: function (jqXHR, settings) {
			$('#lb_bg #errorContainer').hide();
			$('#lb_bg').stop().show().animate({'opacity' : 1}, 500);
	 		$('#lb_bg #lb_loader').stop().show();
			
	        $.xhrPool.push(jqXHR);
			
			_gaq.push(['_trackPageview', data.href]);
		}
	})
	.done(function (thisdata, textStatus, jqCHR) {
		$(this).html(thisdata);
		
		// Load general event listeners
		loadEventListeners();
		
		
		// Load specific event listeners
		if (data.href.indexOf('world') !== -1) {
			loadWorldEventListeners();
		} else if (data.href.indexOf('home') !== -1) {
			socket = io.connect('http://streamwatchr.com');
			loadGridEventListeners(socket);
			// Enable polling artist counts
			doPoll();
		}

		$('#lb_bg').hide();
		// $('#lb_bg #lb_loader').stop().css({'opacity' : 1});
		
	})
	.fail(function (jqXHR, textStatus, errorThrown) {
		// console.log(textStatus);
		// console.log(errorThrown);
		
 		$('#lb_bg #errorContainer').show();
		if (textStatus === "timeout") {
			$("#error").html("Loading is taking longer than normal.");
		} else {
			$("#error").html(jqXHR.responseText);
		}
		$("#errorhref").attr('href', "http://twitter.com/share?text=@streamwatchr "+textStatus);
	})
	.always(function (jqXHR) {
		// handle the status of remaining AJAX requests
        var index = $.inArray(jqXHR, $.xhrPool);
        if (index > -1) {
            $.xhrPool.splice(index, 1);
        }
		
		// Remove active from top menu
		$('ul.menu li a').each( function () {
			$(this).removeClass('active');
		});
		
		// Add active to current menu item and set page title
		$('ul.menu li a#'+data.item).addClass('active');
		$('title').html(data.title);
		
 		$('#lb_bg #lb_loader').stop().hide();
	});
	
}


