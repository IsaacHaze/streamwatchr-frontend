define(['angular'], function(angular) {
  'use strict';

  angular.module('streamwatchr.ilpslogging', [])
    .factory('IlpsLogging', ['$window', '$rootScope', 'ILPSLOGGING_ENABLED', 'ILPSLOGGING_JS_SDK_URL', 'ILPSLOGGING_CONFIG', function($window, $rootScope, ILPSLOGGING_ENABLED, ILPSLOGGING_JS_SDK_URL, ILPSLOGGING_CONFIG) {
      var ready = false;

      if (ILPSLOGGING_ENABLED) {
        (function() {
          var script = document.createElement('script');
          script.src = ILPSLOGGING_JS_SDK_URL;
          var entry = document.getElementsByTagName('script')[0];
          entry.parentNode.insertBefore(script, entry);
        })();

        $window.ILPSLogging_ready = $window.ILPSLogging_ready || [];
        $window.ILPSLogging_ready.push(function(){
          $window.ILPSLogging.init(ILPSLOGGING_CONFIG, function(){
            $rootScope.$apply(function() {
              ready = true;
            });
          });
        });
      }
      return {
        getState: function() {
          if (ready) {
            return $window.ILPSLogging.getState();
          } else {
            return {};
          }
        },

        setState: function (new_state) {
          if (ready) {
            return $window.ILPSLogging.setState(new_state);
          }
        },

        logEvent: function (event_type, event_properties){
          if (ready) $window.ILPSLogging.logEvent(event_type, event_properties);
        }
      };
    }]);
});