define([
  'app'
],
function(app) {
  'use strict';

  return {
    parent: 'layout',
    url: '/about',
    templateUrl: 'partials/about.html',
    controller: function($scope, $rootScope, $location, Pagetitle, IlpsLogging) {
      Pagetitle.set('About');
      IlpsLogging.logEvent('test_event', {});
    }
  };
});