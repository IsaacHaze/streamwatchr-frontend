define(['angular', 'jquery'], function(angular, jQuery) {
  'use strict';

  angular.module('streamwatchr.scrollticker', [])
    .directive('scrollTicker', ['$timeout', function($timeout) {
      function calculateWidth(element) {
        var width = 0;

        jQuery(element).find('li').each(function(i) {
          var li_width = jQuery(this, i).outerWidth(true);
          width = width + li_width + 10;
        });

        return width;
      }

      return {
        restrict: 'A',
        link: function($scope, element, attributes) {
          $scope.$watch('stats.lyrics', function(newLyrics, oldLyrics) {
            $timeout(function(){
              element.css('width', calculateWidth(element) + 'px');
            }, 1500);
          });
        }
      };
    }]);
});