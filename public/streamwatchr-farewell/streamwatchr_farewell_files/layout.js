define([
  'app'
],
function(app) {
  'use strict';

  return {
    abstract: true,
    templateUrl: 'partials/layout.html',
    controller: function($scope, $rootScope, $location, Pagetitle) {
    }
  };
});