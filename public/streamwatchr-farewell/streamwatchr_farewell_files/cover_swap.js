define(['app'], function(app){
  'use strict';

  app.directive('coverSwap', function() {
    return {
      restrict: 'A',
      link: function($scope, element, attributes) {

        var thumb_front = element.find('img.front');
        var thumb_back = element.find('img.back');

        $scope.$watch("song.images[3]", function(newSrc, oldSrc) {
          //console.log(newSrc, oldSrc);
          if (newSrc['#text'] === oldSrc['#text']) {
            if (thumb_front.prop('src').substr(0, 4) !== 'http') {
              thumb_front.prop('src', newSrc['#text']).addClass(newSrc.imgClass);
            }

            return;
          }

          if (element.hasClass('rotated')) {
            thumb_front.prop('src', newSrc['#text']).removeClass('hor ver').addClass(newSrc.imgClass);
            element.removeClass('rotated');
          } else {
            thumb_back.prop('src', newSrc['#text']).removeClass('hor ver').addClass(newSrc.imgClass);
            element.addClass('rotated');
          }
        });
      }
    };
  });
});