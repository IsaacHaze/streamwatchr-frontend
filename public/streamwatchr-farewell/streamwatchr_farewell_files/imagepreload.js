define(['angular', 'jquery'], function(angular, jQuery) {
  'use strict';

  angular.module('streamwatchr.imagepreload', [])
    .factory('ImagePreload', function($rootScope, $q) {
      function preload(image_location) {
        var deferred = $q.defer();

        var image = jQuery(new Image())
          .load(function(event) {
            // Resolve the promise and pass along some additional info about
            // the downloaded image
            deferred.resolve({
              src: image_location,
              width: image[0].width,
              height: image[0].height
            });

            // Clear references to the image and event
            image.unbind();
            image = deferred = event = null;
          })
          .error(function(event) {
            // Reject the promise when the image can't be downloaded
            deferred.reject(event);
          })
          .prop('src',  image_location);

          return deferred.promise;
      }

      return {
        preload: preload
      };
    });
});