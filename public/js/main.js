require.config({
  paths: {
    'angular': 'lib/angular',
    'angularRoute': 'lib/angular-route.min',
    'angularAnimate': 'lib/angular-animate.min',
    'angularUIRouter': 'lib/angular-ui-router.min',
    'angularSocketIO': 'lib/angular-socket.min',
    'angularUIBootstrap': 'lib/angular-ui-bootstrap-tpls',
    'jquery': 'lib/jquery.min',
    'socketio': 'lib/socket.io',
    'd3': 'lib/d3.min',
    'nv': 'lib/nv.d3.min'
  },
  shim: {
    'angular': {
      exports: 'angular',
      deps: ['jquery']
    },
    'angularRoute': ['angular'],
    'angularAnimate': ['angular'],
    'angularUIRouter': ['angular'],
    'angularUIBootstrap': ['angular'],
    'angularSocketIO': ['angular', 'socketio'],
    'jquery': {
      exports: 'jQuery'
    },
    'socketio': {
      exports: 'io'
    },
    'nv': ['d3']
  }
});

require(['app', 'angular', 'routes'], function(app, angular) {
  'use strict';

  angular.bootstrap(document, ['app']);
});