define([
  'angular',
  'app'
],
function(angular, app) {
'use strict';

  return {
    parent: 'layout',
    url: '/unexpected?entity',
    resolve: {
      items: ['$stateParams', '$http', function($stateParams, $http) {
          if ($stateParams.entity === 'artist') {
            return $http.get('/api/off-the-beaten-track/artists');
          } else {
            return $http.get('/api/off-the-beaten-track/songs');
          }
      }]
    },
    templateUrl: function($stateParams) {
      if ($stateParams.entity === 'artist') {
        return 'partials/unexpected-artist.html';
      } else {
        return 'partials/unexpected-song.html';
      }
    },
    controller: ['$stateParams', '$scope', '$rootScope', '$location', '$http', 'Pagetitle', 'ImagePreload', 'IlpsLogging', 'items', function($stateParams, $scope, $rootScope, $location, $http, Pagetitle, ImagePreload, IlpsLogging, items) {
      Pagetitle.set('Unexpected music on the planet');
      
      if (!$stateParams.entity) {
        $scope.entity = 'song';
      } else {
        $scope.entity = $stateParams.entity;
      }

      var itemIds = [];
      $scope.items = [];
      angular.forEach(items.data.docs, function(item, position){
        $scope.items[position] = item;

        // Load and set the image dimensions, if we have a URL
        if (item.artist_image && item.artist_image[3]['#text'] !== "") {
          ImagePreload.preload(item.artist_image[3]['#text']).then(function(data) {
            // Pass the image's actual dimensions (used to determine styling)
            $scope.items[position].artist_image[3].width = data.width;
            $scope.items[position].artist_image[3].height = data.height;

            if (data.width > data.height) {
              $scope.items[position].artist_image[3].imgClass = 'ver';
            } else {
              $scope.items[position].artist_image[3].imgClass = 'hor';
            }
          });

          var stats_url;
          var req_params = {trendline: 1};
          if ($scope.entity === 'song') {
            stats_url = '/api/song-stats-overall';
            req_params.id = item.song_popularity_id;
          } else {
            stats_url = '/api/artist-stats-overall';
            req_params.id = item.mbId;
          }

          // Get the song's stats
          $http.get(stats_url, {params: req_params }).then(function(success){
            $scope.items[position].stats = success.data;
          });
        }

        itemIds.push(item._id);
      });

      if ($scope.entity === 'song') {
        IlpsLogging.logEvent('song_list', { doc_ids: itemIds });
      } else {
        IlpsLogging.logEvent('artist_list', { doc_ids: itemIds });
      }

      $scope.chartXFunction = function() {
        return function(d) { return new Date(d[0]); };
      };

      $scope.chartYFunction = function() {
        return function(d) { return d[1]; };
      };

      $scope.chartPathColor = function() {
        return function(d, i) { return '#40ac5a'; };
      };
    }]
  };
});