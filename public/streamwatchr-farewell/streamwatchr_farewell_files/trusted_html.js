define(['app'], function (app) {
  'use strict';

  app.filter('trusted_html', ['$sce', function($sce) {
    return function(inputHtml) {
      return $sce.trustAsHtml(inputHtml);
    };
  }]);
});