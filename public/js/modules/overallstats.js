define(['angular'], function(angular) {
  'use strict';

  angular.module('streamwatchr.overallstats', [])
    .controller('OverallStatsController', ['$scope', 'CurrentlyPlaying', function($scope, CurrentlyPlaying) {
      $scope.stats = CurrentlyPlaying.stats;
    }]);
});