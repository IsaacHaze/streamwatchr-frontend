// Correct the ratio of images
$('#popular_now .thumb').imagesLoaded().progress( function(instance, image) {
	if (image.img.width > image.img.height) {
		image.img.className = 'ver';
	} else {
		image.img.className = 'hor';
	}
});

// Draw graphs
var margin = {top: 0, right: 0, bottom: 0, left: 0},
	width = 161 - margin.left - margin.right,
	height = 40 - margin.top - margin.bottom;

var parseDate = d3.time.format("%d-%b-%y").parse;

var x = d3.time.scale()
	.range([0, width]);

var y = d3.scale.linear()
	.range([height, 0]);

var line = d3.svg.line()
	.x(function(d) { return x(d.idx); })
	.y(function(d) { return y(d.value); });

$('li.row li.statistics').each(function(index){
	var $this = $(this);
	
	var svg = d3.select(this)
				.append("svg")
					.attr("width", width + margin.left + margin.right)
					.attr("height", height + margin.top + margin.bottom)
				.append("g")
					.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
				
	d3.json('/'+$this.data('type')+'-stats-overall.json?trendline=1&id='+$this.data('id'), function(error, data) {
		var counter = 0;
		data.forEach(function(val, i) {
			data[i] = {idx: i, value: val[1]}
		});

		x.domain(d3.extent(data, function(d) { return d.idx; }));
		y.domain(d3.extent(data, function(d) { return d.value; }));

		svg.append("path")
			.datum(data)
			.attr("class", "line")
			.attr("d", line);
	});
});


// Switch between artists and songs
$("ul#switch li a").click(function(e) {
		e.preventDefault();
		$(this).parent().siblings().removeClass('active');
		$(this).parent().addClass('active');

		_gaq.push(['_trackPageview', $(this).attr('href')]);

		// history.pushState({ path: this.path }, '', this.href);
		$('#container').empty(); // clear event listeners
		
		$('#container').load(this.href, function() {
			// Load general event listeners
			loadEventListeners();
		});
	  
});
