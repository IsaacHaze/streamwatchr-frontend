define(['angular'], function(angular) {
  'use strict';

  return angular.module('streamwatchr.config', [])
    .constant('ILPSLOGGING_ENABLED', true)
    .constant('ILPSLOGGING_JS_SDK_URL', 'http://zookst22.science.uva.nl:8008/jssdk/ilpslogging-0.2.min.js')
    .constant('ILPSLOGGING_CONFIG', {
      api_url: 'http://zookst22.science.uva.nl:8008',
      project_key: 'UmyV3VQqwQHeFEmlZr4yChkodwR9oeiUMFOcI7lh4EM',
      log_mouse_movements: false,
      log_mouse_clicks: true,
      post_events_queue_on_browser_close: true,
      log_browser_close: true,
      debug: false
    });
});
