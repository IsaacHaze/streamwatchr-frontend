define([
  'angular',
  'angularRoute',
  'angularAnimate',
  'angularUIRouter',
  'angularUIBootstrap',
  'angularSocketIO',
  'config',
  'modules/pagetitle',
  'modules/ilpslogging',
  'modules/currentlyplaying',
  'modules/overallstats',
  'modules/lyrics',
  'modules/player',
  'modules/youtube',
  'modules/imagepreload',
  'modules/nvd3',
  'directives/scrollticker'
],
function(angular) {
  'use strict';

  return angular.module('app', [
      'streamwatchr.config',
      'streamwatchr.ilpslogging',
      'streamwatchr.player',
      'streamwatchr.lyrics',
      'streamwatchr.imagepreload',
      'streamwatchr.currentlyplaying',
      'streamwatchr.pagetitle',
      'streamwatchr.overallstats',
      'nvd3ChartDirectives',
      'ngRoute',
      'ngAnimate',
      'ui.router',
      'ui.bootstrap',
      'btford.socket-io'
    ])
    .run(['$rootScope', '$state', '$stateParams', 'IlpsLogging', function($rootScope, $state, $stateParams, IlpsLogging){
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;

      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        $rootScope.showLoadingOverlay = true;
      });

      $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        $rootScope.showLoadingOverlay = false;

        IlpsLogging.logEvent('page', {
          'from_page': fromState.name,
          'to_page': toState.name
        });
        
        var logState = IlpsLogging.getState();
        logState.page = toState.name;
        IlpsLogging.setState(logState);
      });
    }]);
});