define([
  'app',
  'states/layout',
  'states/home',
  'states/hot',
  'states/unexpected',
  'states/about',
  'states/song_details',
  'states/artist_details',

  'filters/truncate',
  'filters/trusted_html',

  'directives/bind_once'
],
function(app, LayoutState, HomeState, HotState, Unexpected, About, SongDetails, ArtistDetails) {
  'use strict';

  app.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', function($stateProvider, $locationProvider, $urlRouterProvider){
    //$locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/');
    $stateProvider
      .state('layout', LayoutState)
      .state('home', HomeState)
      .state('home.song_details', {
        url: 'song/:songId',
        views: {
          'modal@layout': SongDetails
        },
        data: {
          collection: 'currently'
        }
      })
      .state('home.artist_details', {
        url: '/artist/:artistId',
        views: {
          'modal@layout': ArtistDetails
        },
        data: {
          collection: null
        }
      })
      .state('hot', HotState)
      .state('hot.song_details', {
        url: '/song/:songId',
        views: {
          'modal@layout': SongDetails
        },
        data: {
          collection: null
        }
      })
      .state('hot.artist_details', {
        url: '/artist/:artistId',
        views: {
          'modal@layout': ArtistDetails
        },
        data: {
          collection: null
        }
      })
      .state('unexpected', Unexpected)
      .state('unexpected.song_details', {
        url: '/song/:songId',
        views: {
          'modal@layout': SongDetails
        },
        data: {
          collection: null
        }
      })
      .state('unexpected.artist_details', {
        url: '/artist/:artistId',
        views: {
          'modal@layout': ArtistDetails
        },
        data: {
          collection: null
        }
      })
      .state('about', About);
  }]);
});