define(['angular'], function(angular) {
  'use strict';

  angular.module('streamwatchr.currentlyplaying', [])
    .factory('CurrentlyPlaying', ['$http', '$timeout', '$log', 'socketFactory', 'IlpsLogging', function($http, $timeout, $log, socketFactory, IlpsLogging) {
      var _socket;
      var stats = {
        totalTweetCount: 0,
        totalUniqueArtistsCount: 0,
        lastPlayedSong: {},
        lyrics: []
      };

      var periodicOverallStatsUpdater = null;
      var periodicLyricsUpdater = null;

      function socketConnect() {
        $log.debug('Connecting to socket connection...');
        _socket = socketFactory();
        return _socket;
      }

      function socketDisconnect() {
        $log.debug('Disconnecting from socket connection...');
        _socket.disconnect();
      }

      function updateOverallStats() {
        var promise = $http({ url: '/api/overall-stats' })
          .then(function(success) {
              stats.totalTweetCount = success.data.tweet_count;
              stats.totalUniqueArtistsCount = success.data.unique_artists_count;

              return success;
          });

        return promise;
      }

      function startPeriodicOverallStatsUpdate(delay_ms) {
        delay_ms = delay_ms || 60000;

        periodicOverallStatsUpdater = $timeout(updateOverallStats, delay_ms, false);

        periodicOverallStatsUpdater.then(function() {
          startPeriodicOverallStatsUpdate(delay_ms);
        });
      }

      function stopPeriodicOverallStatsUpdate() {
        $log.debug('Stopping periodic stats updates');

        $timeout.cancel(periodicOverallStatsUpdater);
        periodicOverallStatsUpdater = null;
      }

      function updateTweetCountOnPlay(max_update_every_ms) {
        if (typeof(max_update_every_ms) === 'undefined') max_update_every_ms = 100;

        var recievedPlays = 0;
        var lastUpdate = new Date();
        
        _socket.on('song_play', function(data) {
          recievedPlays++;
          var now = new Date();

          if (now - lastUpdate > max_update_every_ms) {
            if (stats.totalTweetCount !== 0) {
              stats.totalTweetCount = stats.totalTweetCount + recievedPlays;
            }

            recievedPlays = 0;
            lastUpdate = now;
          }
        });
      }

      function updateLastPlayedSong(max_update_every_ms) {
        if (typeof(max_update_every_ms) === 'undefined') max_update_every_ms = 100;

        var lastUpdate = new Date();

        _socket.on('song_play', function(data) {
          var now = new Date();

          if (now - lastUpdate > max_update_every_ms) {
            stats.lastPlayedSong = data;
            lastUpdate = now;
          }
        });
      }

      function getRecentlyPlayedSongs() {
        return $http({ url: '/api/recently-played'});
      }

      function getLatestLyrics() {
        var promise = $http({url: '/api/latest-lyrics'})
          .then(function(success) {
            stats.lyrics = success.data.lyrics;

            return success;
          });

        return promise;
      }

      // Get the current Tweet and artist counts
      updateOverallStats();

      // Get the latest lyric Tweets
      getLatestLyrics();

      // Connect to the (web)socket
      socketConnect();

      // Listen for song_play events and update the Tweet counter
      updateTweetCountOnPlay();

      // Listen for song_play events and update the last played song
      updateLastPlayedSong();

      // Periodically fetch the song and artist counts
      startPeriodicOverallStatsUpdate();

      return {
        stats: stats,
        socketConnect: socketConnect,
        socketDisconnect: socketDisconnect,
        updateOverallStats: updateOverallStats,
        startPeriodicOverallStatsUpdate: startPeriodicOverallStatsUpdate,
        stopPeriodicOverallStatsUpdate: stopPeriodicOverallStatsUpdate,
        getRecentlyPlayedSongs: getRecentlyPlayedSongs,
        getLatestLyrics: getLatestLyrics
      };
    }]);
});