define(['angular'], function(angular) {
  'use strict';

  angular.module('streamwatchr.pagetitle', [])
    .service('Pagetitle', function($rootScope, $window) {
      $rootScope.$on('$stateChangeStart', function(){
        $window.document.title = 'Streamwatchr';
      });

      return {
        set: function(documentTitle) {
          if (documentTitle) {
            documentTitle = documentTitle += ' - Streamwatchr';
          }
          $window.document.title = documentTitle;
        },
        get: function() {
          return $window.document.title;
        }
      };
    });
});