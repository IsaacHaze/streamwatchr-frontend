//
// Library holding all of Lightbox code
//
var touchScreen = false;


//LockScroll for lightbox
var pos = "0"; 
var handler = function() {
	$('html, body').scrollTop(pos);
};

var lockScroll = function(stat) {
   	var FF = !(window.mozInnerScreenX == null);
   	if (FF == false) {
		if (stat == true ) {
			$(window).bind('scroll', handler);
			document.onmousewheel = function(){ stopWheel(); } /* IE7, IE8 */
			if(document.addEventListener){ /* Chrome, Safari, Firefox */
			    document.addEventListener('DOMMouseScroll', stopWheel, false);
			}			 
			function stopWheel(e){
			    if(!e){ e = window.event; } /* IE7, IE8, Chrome, Safari */
			    if(e.preventDefault) { e.preventDefault(); } /* Chrome, Safari, Firefox */
			    e.returnValue = false; /* IE7, IE8 */
			}
	    } else {
	    	$(window).unbind('scroll', handler);
			document.onmousewheel = null;  /* IE7, IE8 */
			if(document.addEventListener){ /* Chrome, Safari, Firefox */
			    document.removeEventListener('DOMMouseScroll', stopWheel, false);
			}
		}
	} else {
	  	if (stat == true ) {
	  	 	$(window).bind('scroll', handler);
	    } else {
	    	 $(window).unbind('scroll', handler); 
		}
    }
}


//lightbox 
var resizeLightbox = function() {
	if ($('#lb').hasClass('active')) {
		var contentHeight = $('#lb .lb_content').height();
		var lbHeight = $(window).height() - $('#sitewide').height() - $('#footer').height() - $('#lb_center_balk').height();
		if ($('#player').hasClass('active')) {
			lbHeight = lbHeight - $('#player').height();
		}
 		if (contentHeight < lbHeight) { 
 			lbHeight = contentHeight; 
 			$('#lb .lb_content .lb_inner').css({'padding-bottom' : 30});
 		} else {
 			if ($('#player').hasClass('active')) {
				$('#lb .lb_content .lb_inner').css({'padding-bottom' : $('#player').height() + 30});
			} else {
				$('#lb .lb_content .lb_inner').css({'padding-bottom' : 30});
			}
 		}
 		if ($('#player').hasClass('active')) {
			lbHeight = lbHeight + $('#player').height();
		}
		$('#lb_center_frame').css({'height' : lbHeight});
	}
}

var lightboxInnerScroll = function() {
	if (touchScreen == false ) {
		$('#lb_center_frame').bind('mousewheel DOMMouseScroll', function(e) {
		    var scrollTo = null;
		    if (e.type == 'mousewheel') {
		        scrollTo = (e.originalEvent.wheelDelta * -1);
		    }
		    else if (e.type == 'DOMMouseScroll') {
		        scrollTo = 40 * e.originalEvent.detail;
		    }
		    if (scrollTo) {
		        e.preventDefault();
		        $(this).scrollTop(scrollTo + $(this).scrollTop());
		    }
		});
	} else {
		$("#lb_center_frame").niceScroll();
	}
}

var lightboxInnerBackFunc = function() {
	$('#lb #lb_balk #lb_back').click(function(e) {
	   closeLightbox('no_bg');
	   var target = $(this).data('target');
	   setTimeout(function() {
		   openLightbox(target, 'popular');
	   }, 1000);
	});
}

var lightboxInnerFuncArtistBio = function(target) {
	// Resolve whether we get artist or song info pages 
	// from the URL
	var splits = target.match(/(artist|song)-info\/(.+)/);
	var entitytype = splits[1]; // artist or song
	var entityid = splits[2]; // artist mbId or song _id
	$('#lb .lb_content .compare_with .compare.new').click(function(e) {
		$(this).find('.text').hide();
		$(this).find('form#compare').show();
		$(this).find('form#compare input').focus();
	});
	$("#lb .lb_content .listing ul li .value_balk").each(function(i) {
		$(this).find('.procent').css({'width' : $(this).data('value')+'%'});
	});
	
    // Load the graph
    drawChart(entitytype, entityid);
    
    // Do other stuff
	var scrollId = 0;
	var scrollDirection = 'down';
	var timeoutDown= 0;
	var stopScrollListingTimeout = 0;
	if (touchScreen == false) {
	    var scrollListing = function(e) {
	      stopScrollListingTimeout = setInterval(function() {
	         var height = parseInt($(scrollId).find('ul').height()) - 175;
	         var scrollTop = parseInt($(scrollId).find('ul').css('margin-top'));
	         if (scrollDirection == 'down') { scrollTop = scrollTop - 10; }
	         if (scrollDirection == 'up') { scrollTop = scrollTop + 10; }
	         if (scrollTop < (height - (2 * height)) ) {scrollTop = height - (2 * height); }
	         if (scrollTop > 0 ) {scrollTop = 0; }
	         $(scrollId).find('ul').css({'margin-top' : scrollTop});
	      }, 50);
	    };
	    $('#lb .lb_content .listing .scroll_buttons .down').mousedown(function(e) {
	    	scrollId = '#'+$(this).parent().parent().attr('id');
	    	scrollDirection = 'down';
	    	scrollListing();
		});
	    $('#lb .lb_content .listing .scroll_buttons .up').mousedown(function(e) {
	    	scrollId = '#'+$(this).parent().parent().attr('id');
	    	scrollDirection = 'up';
	    	scrollListing();
		});
	    $('#lb .lb_content .listing .scroll_buttons .down, #lb .lb_content .listing .scroll_buttons .up').mouseup(function(e) {
	    	clearInterval(stopScrollListingTimeout);
		});
	    $('#lb .lb_content .listing .scroll_buttons .down, #lb .lb_content .listing .scroll_buttons .up').mouseleave(function(e) {
	    	clearInterval(stopScrollListingTimeout);
		});
	} else {
		var scrollTouchScreenListing = function(e) {
	         var height = parseInt($(scrollId).find('ul').height()) - 175;
	         var scrollTop = parseInt($(scrollId).find('ul').css('margin-top'));
	         if (scrollDirection == 'down') { scrollTop = scrollTop - 100; }
	         if (scrollDirection == 'up') { scrollTop = scrollTop + 100; }
	         if (scrollTop < (height - (2 * height)) ) {scrollTop = height - (2 * height); }
	         if (scrollTop > 0 ) {scrollTop = 0; }
	         $(scrollId).find('ul').css({'margin-top' : scrollTop});
		};
		$('#lb .lb_content .listing .scroll_buttons .down').click(function(e) {
	    	scrollId = '#'+$(this).parent().parent().attr('id');
	    	scrollDirection = 'down';
	    	scrollTouchScreenListing();
		});
	    $('#lb .lb_content .listing .scroll_buttons .up').click(function(e) {
	    	scrollId = '#'+$(this).parent().parent().attr('id');
	    	scrollDirection = 'up';
	    	scrollTouchScreenListing();
		});
	}
}

var lightboxInnerFuncPopular = function(backUrl) {
	$("#lb .lb_content ul.popular_listing li.row").click(function(e) {
	   if (e.target.className == 'play-btn play_youtube' || e.target.className == 'play_youtube' ) {
		   showPlayer($(this).find('.play-btn').data('target'));
		   resizeLightbox();
	   } else {
		   closeLightbox('no_bg');
		   var target = $(this).data('target');
		   var backTitle = $('#lb .lb_header_2 h2').html();
		   setTimeout(function() {
			   openLightbox(target, 'artist_bio', backTitle, backUrl);
		   }, 1000);
	   }
	});
}

var openLightbox = function(target, type, backTitle, backUrl) {
	$.ajax({
		url: target,
		context: $( "#lb_frame" ),
		beforeSend: function (jqXHR, settings) {
			lockScroll(true);
			
			$('#lb').addClass('active');
			$('#lb_bg #errorContainer').hide();
	 		$('#lb_bg #lb_loader').stop().show();
			$('#lb_bg').stop().show().animate({'opacity' : 1}, 500);
			
	        $.xhrPool.push(jqXHR);
			
			_gaq.push(['_trackPageview', target]);
		}
	})
	.done(function (data, textStatus, jqXHR) {
		$(this).html(data);
		
		$('#lb').show().css({'left' : ($(window).width() * 2)});
 		if (type == 'artist_bio') {
 			lightboxInnerFuncArtistBio(target);
 			$('#lb #lb_center_balk .artist_bio').show();
 			$('#lb #lb_center_balk .popular').hide();
 		} else if (type == 'popular') {
 			// lightboxInnerFuncPopular(target);
 			$('#lb #lb_center_balk .artist_bio').hide();
 			$('#lb #lb_center_balk .popular').show();
 		}
 		if (!backTitle) {
 			$('#lb #lb_balk #lb_back').hide();
 		} else {
 			$('#lb #lb_balk #lb_back').show().attr('data-target', backUrl);
 			$('#lb #lb_balk #lb_back span.title').html(backTitle);
 			lightboxInnerBackFunc();
 		} 
 		resizeLightbox();
 		lightboxInnerScroll();
 		if ($('#player #show_youtube').hasClass('active')) {
			hideYoutube();
		}
		$('#lb').css({'top' : '-100%', 'left' : '-10%'});
		$('#lb_bg #lb_loader').stop().animate({'opacity' : 0}, 500);
		$('#lb').stop().show().animate({'top' : 50}, 500, function() {});
	})
	.fail(function (jqXHR, textStatus, errorThrown) {
		// console.log(textStatus);
 		$('#lb_bg #errorContainer').show();
		if (textStatus === "timeout") {
			$("#error").html("Loading is taking longer than normal.");
		} else {
			$("#error").html(jqXHR.responseText);
		}
		$("#errorhref").attr('href', "http://twitter.com/share?text=@streamwatchr "+$("#error").find("title"));
	})
	.always(function (jqXHR) {
		// handle the status of remaining AJAX requests
        var index = $.inArray(jqXHR, $.xhrPool);
        if (index > -1) {
            $.xhrPool.splice(index, 1);
        }
		
		$('#lb_bg #lb_loader').stop().animate({'opacity' : 0}, 500);
	});
	
	// lockScroll(true);
	// $('#lb').addClass('active');
	// $('#lb_bg #errorContainer').hide();
	// $('#lb_bg').stop().show().animate({'opacity' : 1}, 500);
	// $( "#lb_frame" ).load( target, function(response, status, xhr) {
	// 	
	// 	$('#lb').show().css({'left' : ($(window).width() * 2)});
	//  		if (type == 'artist_bio') {
	//  			lightboxInnerFuncArtistBio(target);
	//  			$('#lb #lb_center_balk .artist_bio').show();
	//  			$('#lb #lb_center_balk .popular').hide();
	//  		} else if (type == 'popular') {
	//  			// lightboxInnerFuncPopular(target);
	//  			$('#lb #lb_center_balk .artist_bio').hide();
	//  			$('#lb #lb_center_balk .popular').show();
	//  		}
	//  		if (!backTitle) {
	//  			$('#lb #lb_balk #lb_back').hide();
	//  		} else {
	//  			$('#lb #lb_balk #lb_back').show().attr('data-target', backUrl);
	//  			$('#lb #lb_balk #lb_back span.title').html(backTitle);
	//  			lightboxInnerBackFunc();
	//  		} 
	//  		resizeLightbox();
	//  		lightboxInnerScroll();
	//  		if ($('#player #show_youtube').hasClass('active')) {
	// 		hideYoutube();
	// 	}
	//  		$('#lb').css({'top' : '-100%', 'left' : '-10%'});
	//  		$('#lb_bg #lb_loader').stop().animate({'opacity' : 0}, 500);
	// 	$('#lb').stop().show().animate({'top' : 50}, 500, function() {});
	// })
}

var closeLightbox = function(option) {
	lockScroll(false);
	$('#lb').removeClass('active');
	if (option != 'no_bg') {
		$('#lb_bg').stop().animate({'opacity' : 0}, 500, function() {
			$('#lb_bg').hide();
			$('#lb_bg #lb_loader').stop().css({'opacity' : 1});
		});
	} else {
		$('#lb_bg #lb_loader').stop().animate({'opacity' : 1}, 300);
	}
	$('#lb').stop().animate({'top' : '-100%'}, 500, function() {
		$('#lb').hide();
	});
}
